﻿using MojTimskiProjekt.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.ViewModels
{
    public partial class BlagajnikViewModel
    {
        public BlagajnikViewModel()
        {
            Racun = new HashSet<Racun>();
        }
        
        public int Id { get; set; }
        public int IdNaplatneKućice { get; set; }
        public int? IdZaposlenika { get; set; }

        public NaplatnaKucica IdNaplatneKućiceNavigation { get; set; }
        public Zaposlenik IdZaposlenikaNavigation { get; set; }
        public ICollection<Racun> Racun { get; set; }
    }
}
