﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.ViewModels
{
    public class ObjektViewModel
    {
        public int Id { get; set; }

        public string Naziv { get; set; }

        public string Lokacija { get; set; }

        public int IdDionice { get; set; }

        public int? IdVrsteObjekta { get; set; }
    }
}
