﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.ViewModels
{
    public class NaplatnaPostajaViewModel
    {
        public int Id { get; set; }

        public string Lokacija { get; set; }

        public string Vrsta { get; set; }
        
        public int IdDionice { get; set; }

    }
}