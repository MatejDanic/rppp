﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.ViewModels
{
    public class UpraviteljViewModel
    {
        public int Id { get; set; }

        public string Naziv { get; set; }
        public string Sjediste { get; set; }
        public string Oib { get; set; }
        public string EMail { get; set; }
        public string Kontakt { get; set; }

       
    }
}
