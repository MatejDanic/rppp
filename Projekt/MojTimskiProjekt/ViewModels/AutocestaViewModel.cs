﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.ViewModels
{
    public class AutocestaViewModel
    {
        public int Id { get; set; }

        public string Naziv { get; set; }
       
        public double DuljinaPlan { get; set; }
  
        public double Duljina { get; set; }
       
        public string Pocetak { get; set; }
      
        public string Kraj { get; set; }
    
        public int IdUpravitelja { get; set; }

      
       
    }
}
