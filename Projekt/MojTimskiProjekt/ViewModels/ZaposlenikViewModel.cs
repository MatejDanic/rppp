﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.ViewModels
{
    public class ZaposlenikViewModel
    {
        public int Id { get; set; }

        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Adresa { get; set; }
        public string Oib { get; set; }

        public int IdUpravitelja { get; set; }
        public int IdVrstePosla { get; set; }

        public Upravitelj IdUpraviteljaNavigation { get; set; }
        public VrstaPosla IdVrstePoslaNavigation { get; set; }

    }
}
