﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.ViewModels
{
    public class AlarmViewModel
    {
        public int Id { get; set; }
  
        public int IdIncidenta { get; set; }
    
        public string Naziv { get; set; }

        public string Vrsta { get; set; }

        public Incident IdIncidentaNavigation { get; set; }

        public string LokacijaIncidenta { get; set; }
    }
}
