﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.ViewModels
{
    public class NaplatnePostajeViewModel
    {
        public IEnumerable<NaplatnaPostajaViewModel> NaplatnePostaje { get; set; }
    }
}
