﻿using MojTimskiProjekt.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.ViewModels
{
    public partial class OperaterViewModel
    {

        public int Id { get; set; }
        public int IdDionice { get; set; }
        public int? IdZaposlenika { get; set; }

        public Dionica IdDioniceNavigation { get; set; }
        public Zaposlenik IdZaposlenikaNavigation { get; set; }
    }
}
