﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MojTimskiProjekt.Areas.Autocomplete.Models;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.Areas.Autocomplete.Controllers
{

    [Route("autocomplete/[controller]")]
    public class UpraviteljController : Controller
    {
        private readonly RPPP14Context ctx;
        private readonly AppSettings appData;

        public UpraviteljController(RPPP14Context ctx, IOptionsSnapshot<AppSettings> options)
        {
            this.ctx = ctx;
            appData = options.Value;
        }

        [HttpGet]
        public IEnumerable<IdLabel> Get(string term)
        {
            var query = ctx.Upravitelj
            .Select(m => new IdLabel
            {
                Id = m.Id,
                Label = m.Naziv
            })
            .Where(l => l.Label.Contains(term));

            var list = query.OrderBy(l => l.Label)
            .ThenBy(l => l.Id)
            .Take(appData.AutoCompleteCount)
           .ToList();
            return list;
        }
    }
}