﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MojTimskiProjekt.Areas.Autocomplete.Models;
using MojTimskiProjekt.Models;
namespace MojTimskiProjekt.Areas.Autocomplete.Controllers
{

    [Route("autocomplete/Autocesta")]
    public class AutocestaController : Controller
    {
        private readonly RPPP14Context ctx;
        private readonly AppSettings appData;

        public AutocestaController(RPPP14Context ctx, IOptionsSnapshot<AppSettings> options)
        {
            this.ctx = ctx;
            appData = options.Value;
        }

        [HttpGet]
        public IEnumerable<IdLabel> Get(string term)
        {
            var query = ctx.Autocesta
                            .Select(m => new IdLabel
                            {
                                Id = m.IdUpravitelja,
                                Label = m.IdUpraviteljaNavigation.Naziv
                            })
                            .Where(l => l.Label.Contains(term));

            var list = query.OrderBy(l => l.Label)
                            .ThenBy(l => l.Id)
                            .Take(appData.AutoCompleteCount)
                            .ToList();
            return list;
        }
    }
}