﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MojTimskiProjekt.Areas.Autocomplete.Models;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.Areas.Autocomplete.Controllers
{

    [Route("autocomplete/Objekt")]
    public class ObjektController : Controller
    {
        private readonly RPPP14Context ctx;
        private readonly AppSettings appData;

        public ObjektController(RPPP14Context ctx, IOptionsSnapshot<AppSettings> options)
        {
            this.ctx = ctx;
            appData = options.Value;
        }

        [HttpGet]
        public IEnumerable<IdLabel> Get(string term)
        {
            var query = ctx.Objekt
            .Select(m => new IdLabel
            {
                Id = m.Id,
                Label = m.Naziv
            })
            .Where(l => l.Label.Contains(term));

            var list = query.OrderBy(l => l.Label)
            .ThenBy(l => l.Id)
            .Take(appData.AutoCompleteCount)
            .ToList();
            return list;
        }
    }
}