﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.Areas.Autocomplete.Models;

namespace MojTimskiProjekt.Areas.Autocomplete.Controllers
{
    [Route("autocomplete/[controller]")]
    public class DionicaController : Controller
    {

        private readonly RPPP14Context ctx;
        private readonly AppSettings appData;


        public DionicaController(RPPP14Context ctx, IOptionsSnapshot<AppSettings> options)
        {
            this.ctx = ctx;
            appData = options.Value;
        }

        [HttpGet]
        public IEnumerable<IdLabel> Get(string term)
        {
            var query = ctx.Dionica
                            .Select(m => new IdLabel
                            {
                                Id = m.IdAutoceste,
                                Label = m.IdAutocesteNavigation.Naziv
                            })
                            .Where(l => l.Label.Contains(term));

            var list = query.OrderBy(l => l.Label)
                            .ThenBy(l => l.Id)
                            .Take(appData.AutoCompleteCount)
                            .ToList();
            return list;
        }
    }
}