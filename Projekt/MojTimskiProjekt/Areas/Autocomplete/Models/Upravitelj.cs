﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Areas.Autocomplete.Models
{

    [DataContract]
    public class Upravitelj
    {
        [DataMember(Name = "label")]
        public string Label { get; set; }
        [DataMember(Name = "id")]
        public int Id { get; set; }
        public Upravitelj() { }
        public Upravitelj(int id, string label)
        {
            Id = id;
            Label = label;
        }
    }

}
