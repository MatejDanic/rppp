﻿$(function () {
    //$(".datum").datepicker({
    //    // dateFormat: "dd.mm.yy"            
    //});

    $("[data-autocomplete]").each(function (index, element) {
        var url = $(element).data('autocomplete');
        var resultplaceholder = $(element).data('autocomplete-result');
        if (resultplaceholder === undefined)
            resultplaceholder = url;
        $(element).blur(function () {
            if ($(element).val().length === 0) {
                $("[data-autocomplete-result-for='" + resultplaceholder + "']").val('');
            }
        });

        $(element).autocomplete({
            source: "/autocomplete/" + url,
            autoFocus: true,
            minLength: 1,
            select: function (event, ui) {
                $(element).val(ui.item.label);
                console.log(resultplaceholder);
                $("[data-autocomplete-result-for='" + resultplaceholder + "']").val(ui.item.id);
            }
        });
    });




});