﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Models
{
    public class BlagajnikExcel
    {
        public int Id { get; set; }
        public int IdNaplatneKućice { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string OIB { get; set; }
        public int? IdZaposlenika { get; set; }

        public List<String> Racuni { get; set; }
        public NaplatnaKucica IdNaplatneKućiceNavigation { get; set; }
        public Zaposlenik IdZaposlenikaNavigation { get; set; }
        public string NaplatnaKucica { get; set; }
    }
}
