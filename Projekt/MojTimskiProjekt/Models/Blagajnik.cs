﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Blagajnik
    {
        public Blagajnik()
        {
            Racun = new HashSet<Racun>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "ID naplatne kućice je obavezan")]
        public int IdNaplatneKućice { get; set; }
        [Required(ErrorMessage = "ID zaposlenika je obavezan")]
        public int? IdZaposlenika { get; set; }

        public NaplatnaKucica IdNaplatneKućiceNavigation { get; set; }
        public Zaposlenik IdZaposlenikaNavigation { get; set; }
        public ICollection<Racun> Racun { get; set; }
    }
}
