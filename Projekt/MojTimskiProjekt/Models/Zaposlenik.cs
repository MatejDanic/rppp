﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Zaposlenik
    {
        public Zaposlenik()
        {
            Blagajnik = new HashSet<Blagajnik>();
            Operater = new HashSet<Operater>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Upravitelj je obavezan")]
        public int IdUpravitelja { get; set; }
        [Required(ErrorMessage = "Vrsta posla je obavezna")]
        public int IdVrstePosla { get; set; }
        [Required(ErrorMessage = "Ime je obavezno")]
        public string Ime { get; set; }
        [Required(ErrorMessage = "Prezime je obavezno")]
        public string Prezime { get; set; }
        [Required(ErrorMessage = "Adresa je obavezna")]
        public string Adresa { get; set; }
        [Required(ErrorMessage = "OIB je obavezan")]
        public string Oib { get; set; }

        public Upravitelj IdUpraviteljaNavigation { get; set; }
        public ICollection<Blagajnik> Blagajnik { get; set; }
        public ICollection<Operater> Operater { get; set; }
        public VrstaPosla IdVrstePoslaNavigation { get; set; }
    }
}
