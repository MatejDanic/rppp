﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Uredaj
    {
        public Uredaj()
        {
            Incident = new HashSet<Incident>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int IdObjekta { get; set; }
        public string Naziv { get; set; }
        public int? IdVrsteUredaja { get; set; }

        public Objekt IdObjektaNavigation { get; set; }
        public VrstaUredaja IdVrsteUredajaNavigation { get; set; }
        public ICollection<Incident> Incident { get; set; }
    }
}
