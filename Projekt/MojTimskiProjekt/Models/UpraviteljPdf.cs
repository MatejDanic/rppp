﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Models
{
    public class UpraviteljPdf
    {
        public int Id { get; set; }

        public string Naziv { get; set; }

        public string Sjediste { get; set; }

        public int Oib { get; set; }

        public string EMail { get; set; }

        public int Kontakt { get; set; }


        public ICollection<Autocesta> Autocesta { get; set; }
        public ICollection<Obavijest> Obavijest { get; set; }
        public ICollection<Scenarij> Scenarij { get; set; }
        public ICollection<Zaposlenik> Zaposlenik { get; set; }

        public String AutocesteString { get; set; }
    }
}
