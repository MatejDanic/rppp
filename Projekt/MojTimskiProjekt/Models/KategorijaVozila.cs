﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class KategorijaVozila
    {
        public KategorijaVozila()
        {
            Racun = new HashSet<Racun>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string KategorijaVozila1 { get; set; }

        public ICollection<Racun> Racun { get; set; }
    }
}
