﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Obavijest
    {
        public Obavijest()
        {
            VrstaObavijesti = new HashSet<VrstaObavijesti>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Opis { get; set; }
        public DateTime DatumOd { get; set; }
        public DateTime DatumDo { get; set; }
        public int IdUpravitelja { get; set; }
        public int? IdAutoceste { get; set; }

        public Autocesta IdAutocesteNavigation { get; set; }
        public Upravitelj IdUpraviteljaNavigation { get; set; }
        public ICollection<VrstaObavijesti> VrstaObavijesti { get; set; }
    }
}
