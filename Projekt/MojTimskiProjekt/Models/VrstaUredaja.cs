﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class VrstaUredaja
    {
        public VrstaUredaja()
        {
            Uredaj = new HashSet<Uredaj>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Vrsta { get; set; }

        public ICollection<Uredaj> Uredaj { get; set; }
    }
}
