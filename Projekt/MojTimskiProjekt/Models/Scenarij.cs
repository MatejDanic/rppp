﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Scenarij
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Naziv { get; set; }
        public int? IdAlarma { get; set; }
        public int IdUpravitelja { get; set; }
        public string Vrsta { get; set; }
        public string Opis { get; set; }
        public string Lokacija { get; set; }
        public DateTime Datum { get; set; }
        public TimeSpan? Vrijeme { get; set; }

        public Alarm IdAlarmaNavigation { get; set; }
        public Upravitelj IdUpraviteljaNavigation { get; set; }
    }
}
