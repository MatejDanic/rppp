﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class VrstaObavijesti
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Vrsta { get; set; }
        public int IdObavijesti { get; set; }

        public Obavijest IdObavijestiNavigation { get; set; }
    }
}
