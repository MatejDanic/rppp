﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Autocesta
    {
        public Autocesta()
        {
            Dionica = new HashSet<Dionica>();
            Obavijest = new HashSet<Obavijest>();
            ProslaStanja = new HashSet<ProslaStanja>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Naziv je obavezan")]
        public string Naziv { get; set; }
        [Required(ErrorMessage = "Duljina je obavezna")]
        public double DuljinaPlan { get; set; }
        [Required(ErrorMessage = "Duljina je obavezna")]
        public double Duljina { get; set; }
        [Required(ErrorMessage = "Pocetak je obavezan")]
        public string Pocetak { get; set; }
        [Required(ErrorMessage = "Kraj je obavezan")]
        public string Kraj { get; set; }
        [Required(ErrorMessage = "upravitelj je obavezan")]
        public int IdUpravitelja { get; set; }

        public Upravitelj IdUpraviteljaNavigation { get; set; }
        public ICollection<Dionica> Dionica { get; set; }
        public ICollection<Obavijest> Obavijest { get; set; }
        public ICollection<ProslaStanja> ProslaStanja { get; set; }
    }
}
