﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Models
{
    public class ObjektPdf
    {
        public int Id { get; set; }

        public string Naziv { get; set; }

        public string Lokacija { get; set; }

        public int IdDionice { get; set; }

        public int? IdVrsteObjekta { get; set; }

        public String NazivDionice { get; set; }
        public String VrstaObjekta { get; set; }

        public Dionica IdDioniceNavigation { get; set; }
        public VrstaObjekta IdVrsteObjektaNavigation { get; set; }
        public ICollection<Uredaj> Uredaj { get; set; }

        public String UredajiString { get; set; }
    }
}
