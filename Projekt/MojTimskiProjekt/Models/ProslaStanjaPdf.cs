﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public class ProslaStanjaPdf
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime DatumOd { get; set; }
        public DateTime DatumDo { get; set; }
        public string Opis { get; set; }
        public int? IdAutoceste { get; set; }
        public int? IdDionice { get; set; }
        public String NazivAutoceste { get; set; }
        public String NazivDionice { get; set; }
        public Autocesta IdAutocesteNavigation { get; set; }
        public Dionica IdDioniceNavigation { get; set; }
    }
}
