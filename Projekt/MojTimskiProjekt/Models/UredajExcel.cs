﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Models
{
    public class UredajExcel
    {
        public int Id { get; set; }
        public int IdObjekta { get; set; }
        public string Naziv { get; set; }
        public int? IdVrsteUredaja { get; set; }
    }
}
