﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Alarm
    {
        public Alarm()
        {
            KategorijaAlarma = new HashSet<KategorijaAlarma>();
            Scenarij = new HashSet<Scenarij>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Incident je obavezno")]
        public int IdIncidenta { get; set; }
        [Required(ErrorMessage = "Naziv je obavezan")]
        public string Naziv { get; set; }
        [Required(ErrorMessage = "Vrsta je obavezna")]
        public string Vrsta { get; set; }

        public Incident IdIncidentaNavigation { get; set; }
        public ICollection<KategorijaAlarma> KategorijaAlarma { get; set; }
        public ICollection<Scenarij> Scenarij { get; set; }
    }
}
