﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Models
{
    public class UpraviteljExcel
    {
        public int Id { get; set; }

        public string Naziv { get; set; }

        public string Sjediste { get; set; }

        public int Oib { get; set; }

        public string EMail { get; set; }

        public int Kontakt { get; set; }
    }
}
