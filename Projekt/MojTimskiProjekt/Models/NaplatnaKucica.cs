﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class NaplatnaKucica
    {
        public NaplatnaKucica()
        {
            Blagajnik = new HashSet<Blagajnik>();
            Racun = new HashSet<Racun>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int IdNaplatnePostaje { get; set; }
        public string Vrsta { get; set; }
        public string Naziv { get; set; }

        public NaplatnaPostaja IdNaplatnePostajeNavigation { get; set; }
        public ICollection<Blagajnik> Blagajnik { get; set; }
        public ICollection<Racun> Racun { get; set; }
    }
}
