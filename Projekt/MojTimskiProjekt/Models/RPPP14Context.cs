﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using MojTimskiProjekt.ViewModels;

namespace MojTimskiProjekt.Models
{
    public partial class RPPP14Context : DbContext
    {
       

        public RPPP14Context(DbContextOptions<RPPP14Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Alarm> Alarm { get; set; }
        public virtual DbSet<Autocesta> Autocesta { get; set; }
        public virtual DbSet<Blagajnik> Blagajnik { get; set; }
        public virtual DbSet<Cjenik> Cjenik { get; set; }
        public virtual DbSet<Dionica> Dionica { get; set; }
        public virtual DbSet<Incident> Incident { get; set; }
        public virtual DbSet<KategorijaAlarma> KategorijaAlarma { get; set; }
        public virtual DbSet<KategorijaIncidenta> KategorijaIncidenta { get; set; }
        public virtual DbSet<KategorijaVozila> KategorijaVozila { get; set; }
        public virtual DbSet<NacinPlacanja> NacinPlacanja { get; set; }
        public virtual DbSet<NaplatnaKucica> NaplatnaKucica { get; set; }
        public virtual DbSet<NaplatnaPostaja> NaplatnaPostaja { get; set; }
        public virtual DbSet<Obavijest> Obavijest { get; set; }
        public virtual DbSet<Objekt> Objekt { get; set; }
        public virtual DbSet<Operater> Operater { get; set; }
        public virtual DbSet<ProslaStanja> ProslaStanja { get; set; }
        public virtual DbSet<Racun> Racun { get; set; }
        public virtual DbSet<Scenarij> Scenarij { get; set; }
        public virtual DbSet<Upravitelj> Upravitelj { get; set; }
        public virtual DbSet<Uredaj> Uredaj { get; set; }
        public virtual DbSet<VrstaObavijesti> VrstaObavijesti { get; set; }
        public virtual DbSet<VrstaObjekta> VrstaObjekta { get; set; }
        public virtual DbSet<VrstaPosla> VrstaPosla { get; set; }
        public virtual DbSet<VrstaUredaja> VrstaUredaja { get; set; }
        public virtual DbSet<Zaposlenik> Zaposlenik { get; set; }

       

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alarm>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdIncidenta).HasColumnName("id_incidenta");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(10);

                entity.Property(e => e.Vrsta)
                    .IsRequired()
                    .HasColumnName("vrsta")
                    .HasMaxLength(10);

                entity.HasOne(d => d.IdIncidentaNavigation)
                    .WithMany(p => p.Alarm)
                    .HasForeignKey(d => d.IdIncidenta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Alarm_Incident");
            });

            modelBuilder.Entity<Autocesta>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Duljina).HasColumnName("duljina");

                entity.Property(e => e.DuljinaPlan).HasColumnName("duljina_plan");

                entity.Property(e => e.IdUpravitelja).HasColumnName("id_upravitelja");

                entity.Property(e => e.Kraj)
                    .IsRequired()
                    .HasColumnName("kraj")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pocetak)
                    .IsRequired()
                    .HasColumnName("pocetak")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdUpraviteljaNavigation)
                    .WithMany(p => p.Autocesta)
                    .HasForeignKey(d => d.IdUpravitelja)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Autocesta_Upravitelj");
            });

            modelBuilder.Entity<Blagajnik>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdNaplatneKućice).HasColumnName("id_naplatne_kućice");

                entity.Property(e => e.IdZaposlenika).HasColumnName("id_zaposlenika");

                

                entity.HasOne(d => d.IdNaplatneKućiceNavigation)
                    .WithMany(p => p.Blagajnik)
                    .HasForeignKey(d => d.IdNaplatneKućice)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Blagajnik_Naplatna_kućica");

                entity.HasOne(d => d.IdZaposlenikaNavigation)
                    .WithMany(p => p.Blagajnik)
                    .HasForeignKey(d => d.IdZaposlenika)
                    .HasConstraintName("FK_Blagajnik_Zaposlenik")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Cjenik>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdNaplatnePostaje).HasColumnName("id_naplatne_postaje");

                entity.Property(e => e.Iznos)
                    .HasColumnName("iznos")
                    .HasColumnType("money");

                entity.Property(e => e.Kategorija)
                    .IsRequired()
                    .HasColumnName("kategorija")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MjestoUlaska)
                    .IsRequired()
                    .HasColumnName("mjesto_ulaska")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdNaplatnePostajeNavigation)
                    .WithMany(p => p.Cjenik)
                    .HasForeignKey(d => d.IdNaplatnePostaje)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Cjenik_Naplatna_postaja");
            });

            modelBuilder.Entity<Dionica>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Duljina).HasColumnName("duljina");

                entity.Property(e => e.IdAutoceste).HasColumnName("id_autoceste");

                entity.Property(e => e.IdOperatera).HasColumnName("id_operatera");

                entity.Property(e => e.Kraj)
                    .IsRequired()
                    .HasColumnName("kraj")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pocetak)
                    .IsRequired()
                    .HasColumnName("pocetak")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Stanje)
                    .IsRequired()
                    .HasColumnName("stanje")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAutocesteNavigation)
                    .WithMany(p => p.Dionica)
                    .HasForeignKey(d => d.IdAutoceste)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Dionica_Autocesta");
            });

            modelBuilder.Entity<Incident>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdUredaja).HasColumnName("id_uredaja");

                entity.Property(e => e.Lokacija)
                    .IsRequired()
                    .HasColumnName("lokacija")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Opis)
                    .HasColumnName("opis")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdUredajaNavigation)
                    .WithMany(p => p.Incident)
                    .HasForeignKey(d => d.IdUredaja)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Incident_Uređaj");
            });

            modelBuilder.Entity<KategorijaAlarma>(entity =>
            {
                entity.ToTable("Kategorija_alarma");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdAlarma).HasColumnName("id_alarma");

                entity.Property(e => e.KategorijaAlarma1)
                    .IsRequired()
                    .HasColumnName("kategorija_alarma")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAlarmaNavigation)
                    .WithMany(p => p.KategorijaAlarma)
                    .HasForeignKey(d => d.IdAlarma)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Kategorija_alarma_Alarm");
            });

            modelBuilder.Entity<KategorijaIncidenta>(entity =>
            {
                entity.ToTable("Kategorija_incidenta");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdIncidenta).HasColumnName("id_incidenta");

                entity.Property(e => e.Kategorija)
                    .IsRequired()
                    .HasColumnName("kategorija")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdIncidentaNavigation)
                    .WithMany(p => p.KategorijaIncidenta)
                    .HasForeignKey(d => d.IdIncidenta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Kategorija_incidenta_Incident");
            });

            modelBuilder.Entity<KategorijaVozila>(entity =>
            {
                entity.ToTable("Kategorija_vozila");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.KategorijaVozila1)
                    .IsRequired()
                    .HasColumnName("kategorija_vozila")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NacinPlacanja>(entity =>
            {
                entity.ToTable("Nacin_placanja");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.NacinPlacanja1)
                    .IsRequired()
                    .HasColumnName("nacin_placanja")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<NaplatnaKucica>(entity =>
            {
                entity.ToTable("Naplatna_kucica");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdNaplatnePostaje).HasColumnName("id_naplatne_postaje");

                entity.Property(e => e.Vrsta)
                    .HasColumnName("vrsta")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdNaplatnePostajeNavigation)
                    .WithMany(p => p.NaplatnaKucica)
                    .HasForeignKey(d => d.IdNaplatnePostaje)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Naplatna_kućica_Naplatna_postaja");
            });

            modelBuilder.Entity<NaplatnaPostaja>(entity =>
            {
                entity.ToTable("Naplatna_postaja");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdDionice).HasColumnName("id_dionice");

                entity.Property(e => e.Lokacija)
                    .IsRequired()
                    .HasColumnName("lokacija")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vrsta)
                    .IsRequired()
                    .HasColumnName("vrsta")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdDioniceNavigation)
                    .WithMany(p => p.NaplatnaPostaja)
                    .HasForeignKey(d => d.IdDionice)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Naplatna_postaja_Dionica");
            });

            modelBuilder.Entity<Obavijest>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DatumDo)
                    .HasColumnName("datum_do")
                    .HasColumnType("date");

                entity.Property(e => e.DatumOd)
                    .HasColumnName("datum_od")
                    .HasColumnType("date");

                entity.Property(e => e.IdAutoceste).HasColumnName("id_autoceste");

                entity.Property(e => e.IdUpravitelja).HasColumnName("id_upravitelja");

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasColumnName("opis")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAutocesteNavigation)
                    .WithMany(p => p.Obavijest)
                    .HasForeignKey(d => d.IdAutoceste)
                    .HasConstraintName("FK_Obavijest_Autocesta");

                entity.HasOne(d => d.IdUpraviteljaNavigation)
                    .WithMany(p => p.Obavijest)
                    .HasForeignKey(d => d.IdUpravitelja)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Obavijest_Upravitelj");
            });

            modelBuilder.Entity<Objekt>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdDionice).HasColumnName("id_dionice");

                entity.Property(e => e.IdVrsteObjekta).HasColumnName("id_vrste_objekta");

                entity.Property(e => e.Lokacija)
                    .IsRequired()
                    .HasColumnName("lokacija")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdDioniceNavigation)
                    .WithMany(p => p.Objekt)
                    .HasForeignKey(d => d.IdDionice)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Objekt_Dionica");

                entity.HasOne(d => d.IdVrsteObjektaNavigation)
                    .WithMany(p => p.Objekt)
                    .HasForeignKey(d => d.IdVrsteObjekta)
                    .HasConstraintName("FK_Objekt_Vrsta_objekta");
            });

            modelBuilder.Entity<Operater>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();
                entity.Property(e => e.IdDionice).HasColumnName("id_dionice");

                entity.Property(e => e.IdZaposlenika).HasColumnName("id_zaposlenika");

               

                entity.HasOne(d => d.IdDioniceNavigation)
                    .WithMany(p => p.Operater)
                    .HasForeignKey(d => d.IdDionice)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Upravitelj_Dionica");

                entity.HasOne(d => d.IdZaposlenikaNavigation)
                    .WithMany(p => p.Operater)
                    .HasForeignKey(d => d.IdZaposlenika)
                    .HasConstraintName("FK_Operater_Zaposlenik")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<ProslaStanja>(entity =>
            {
                entity.ToTable("Prosla_stanja");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DatumDo)
                    .HasColumnName("datum_do")
                    .HasColumnType("date");

                entity.Property(e => e.DatumOd)
                    .HasColumnName("datum_od")
                    .HasColumnType("date");

                entity.Property(e => e.IdAutoceste).HasColumnName("id_autoceste");

                entity.Property(e => e.IdDionice).HasColumnName("id_dionice");

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasColumnName("opis")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAutocesteNavigation)
                    .WithMany(p => p.ProslaStanja)
                    .HasForeignKey(d => d.IdAutoceste)
                    .HasConstraintName("FK_Prosla_stanja_Autocesta");

                entity.HasOne(d => d.IdDioniceNavigation)
                    .WithMany(p => p.ProslaStanja)
                    .HasForeignKey(d => d.IdDionice)
                    .HasConstraintName("FK_Prosla_stanja_Dionica");
            });

            modelBuilder.Entity<Racun>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DatumIzlaz)
                    .HasColumnName("datum_izlaz")
                    .HasColumnType("date");

                entity.Property(e => e.DatumUlaz)
                    .HasColumnName("datum_ulaz")
                    .HasColumnType("date");

                entity.Property(e => e.IdBlagajnika).HasColumnName("id_blagajnika");

                entity.Property(e => e.IdKategorijeVozila).HasColumnName("id_kategorije_vozila");

                entity.Property(e => e.IdNacinaPlacanja).HasColumnName("id_nacina_placanja");

                entity.Property(e => e.IdNaplatneKucice).HasColumnName("id_naplatne_kucice");

                entity.Property(e => e.Iznos)
                    .HasColumnName("iznos")
                    .HasColumnType("money");

                entity.Property(e => e.MjestoIzlaska)
                    .IsRequired()
                    .HasColumnName("mjesto_izlaska")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MjestoUlaska)
                    .IsRequired()
                    .HasColumnName("mjesto_ulaska")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RegistracijskeOznake)
                    .IsRequired()
                    .HasColumnName("registracijske_oznake")
                    .HasMaxLength(8)
                    .IsUnicode(false);
                

                entity.HasOne(d => d.IdBlagajnikaNavigation)
                    .WithMany(p => p.Racun)
                    .HasForeignKey(d => d.IdBlagajnika)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Racun_Blagajnik");

                entity.HasOne(d => d.IdKategorijeVozilaNavigation)
                    .WithMany(p => p.Racun)
                    .HasForeignKey(d => d.IdKategorijeVozila)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Kategorija_vozila_Racun");

                entity.HasOne(d => d.IdNacinaPlacanjaNavigation)
                    .WithMany(p => p.Racun)
                    .HasForeignKey(d => d.IdNacinaPlacanja)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Racun_Nacin_placanja");

                entity.HasOne(d => d.IdNaplatneKuciceNavigation)
                    .WithMany(p => p.Racun)
                    .HasForeignKey(d => d.IdNaplatneKucice)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Racun_Naplatna_kucica");
            });

            modelBuilder.Entity<Scenarij>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Datum)
                    .HasColumnName("datum")
                    .HasColumnType("date");

                entity.Property(e => e.IdAlarma).HasColumnName("id_alarma");

                entity.Property(e => e.IdUpravitelja).HasColumnName("id_upravitelja");

                entity.Property(e => e.Lokacija)
                    .IsRequired()
                    .HasColumnName("lokacija")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Naziv)
                    .HasColumnName("naziv")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasColumnName("opis")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Vrijeme).HasColumnName("vrijeme");

                entity.Property(e => e.Vrsta)
                    .IsRequired()
                    .HasColumnName("vrsta")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAlarmaNavigation)
                    .WithMany(p => p.Scenarij)
                    .HasForeignKey(d => d.IdAlarma)
                    .HasConstraintName("FK_Scenarij_Alarm");

                entity.HasOne(d => d.IdUpraviteljaNavigation)
                    .WithMany(p => p.Scenarij)
                    .HasForeignKey(d => d.IdUpravitelja)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Scenarij_Upravitelj");
            });

            modelBuilder.Entity<Upravitelj>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.EMail)
                    .HasColumnName("e-mail")
                    .HasMaxLength(50);

                entity.Property(e => e.Kontakt).HasColumnName("kontakt");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(50);

                entity.Property(e => e.Oib).HasColumnName("OIB");

                entity.Property(e => e.Sjediste)
                    .IsRequired()
                    .HasColumnName("sjediste")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Uredaj>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdObjekta).HasColumnName("id_objekta");

                entity.Property(e => e.IdVrsteUredaja).HasColumnName("id_vrste_uredaja");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasColumnName("naziv")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdObjektaNavigation)
                    .WithMany(p => p.Uredaj)
                    .HasForeignKey(d => d.IdObjekta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Uredaj_Objekt");

                entity.HasOne(d => d.IdVrsteUredajaNavigation)
                    .WithMany(p => p.Uredaj)
                    .HasForeignKey(d => d.IdVrsteUredaja)
                    .HasConstraintName("FK_Uredaj_Vrsta_uredaja");
            });

            modelBuilder.Entity<VrstaObavijesti>(entity =>
            {
                entity.ToTable("Vrsta_obavijesti");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdObavijesti).HasColumnName("id_obavijesti");

                entity.Property(e => e.Vrsta)
                    .IsRequired()
                    .HasColumnName("vrsta")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdObavijestiNavigation)
                    .WithMany(p => p.VrstaObavijesti)
                    .HasForeignKey(d => d.IdObavijesti)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Vrsta_obavijesti_obavijest");
            });

            modelBuilder.Entity<VrstaObjekta>(entity =>
            {
                entity.ToTable("Vrsta_objekta");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Vrsta)
                    .IsRequired()
                    .HasColumnName("vrsta")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VrstaPosla>(entity =>
            {
                entity.ToTable("Vrsta_posla");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();


                entity.Property(e => e.Vrsta)
                    .IsRequired()
                    .HasColumnName("vrsta")
                    .HasMaxLength(50)
                    .IsUnicode(false);
                
            });

            modelBuilder.Entity<VrstaUredaja>(entity =>
            {
                entity.ToTable("Vrsta_uredaja");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Vrsta)
                    .IsRequired()
                    .HasColumnName("vrsta")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Zaposlenik>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdUpravitelja).HasColumnName("id_upravitelja");

                entity.HasOne(d => d.IdUpraviteljaNavigation)
                    .WithMany(p => p.Zaposlenik)
                    .HasForeignKey(d => d.IdUpravitelja)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Zaposlenik_Upravitelj");

                entity.Property(e => e.IdVrstePosla).HasColumnName("id_vrste_posla");

                entity.HasOne(d => d.IdVrstePoslaNavigation)
                    .WithMany(p => p.Zaposlenik)
                    .HasForeignKey(d => d.IdVrstePosla)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Zaposlenik_Vrsta_posla");

                entity.Property(e => e.Adresa)
                    .IsRequired()
                    .HasColumnName("adresa")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasColumnName("ime")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasColumnName("prezime")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Oib)
                   .IsRequired()
                   .HasColumnName("OIB")
                   .HasMaxLength(11)
                   .IsUnicode(false);
            });
        }

       

        public DbSet<MojTimskiProjekt.ViewModels.AutocestaViewModel> AutocestaViewModel { get; set; }
        public DbSet<MojTimskiProjekt.ViewModels.ObjektViewModel> ObjektViewModel { get; set; }
        public DbSet<MojTimskiProjekt.ViewModels.UredajViewModel> UredajViewModel { get; set; }
    }
}
