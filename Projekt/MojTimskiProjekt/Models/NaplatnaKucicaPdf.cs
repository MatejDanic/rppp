﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Models
{

    public partial class NaplatnaKucicaPdf
    {
       
        public int Id { get; set; }
        public int IdNaplatnePostaje { get; set; }
        public string Vrsta { get; set; }
        public string NazivKucice { get; set; }
        public string NazivPostaje { get; set; }
        public NaplatnaPostaja IdNaplatnePostajeNavigation { get; set; }
        public ICollection<Blagajnik> Blagajnik { get; set; }
        public ICollection<Racun> Racun { get; set; }
        public String RacuniString { get; set; }
    }
}
