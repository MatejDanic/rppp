﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class VrstaObjekta
    {
        public VrstaObjekta()
        {
            Objekt = new HashSet<Objekt>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Vrsta { get; set; }

        public ICollection<Objekt> Objekt { get; set; }
    }
}
