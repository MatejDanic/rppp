﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Operater
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Dionica je obavezna")]
        public int IdDionice { get; set; }
        [Required(ErrorMessage = "Zaposlenik je obavezan")]
        public int? IdZaposlenika { get; set; }

        public Dionica IdDioniceNavigation { get; set; }
        public Zaposlenik IdZaposlenikaNavigation { get; set; }
    }
}
