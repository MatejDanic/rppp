﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class KategorijaAlarma
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string KategorijaAlarma1 { get; set; }
        public int IdAlarma { get; set; }

        public Alarm IdAlarmaNavigation { get; set; }
    }
}
