﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Models
{
    public class AutocestaExcel
    {

        public int Id { get; set; }

        public string Naziv { get; set; }

        public double DuljinaPlan { get; set; }

        public double Duljina { get; set; }

        public string Pocetak { get; set; }

        public string Kraj { get; set; }

        public int IdUpravitelja { get; set; }
      
        public Upravitelj IdUpraviteljaNavigation { get; set; }

        public List<String> Dionice { get; set; }

    }
}
