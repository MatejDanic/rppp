﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class VrstaPosla
    {
        public VrstaPosla()
        {
            Zaposlenik = new HashSet<Zaposlenik>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Vrsta { get; set; }
      

        public ICollection<Zaposlenik> Zaposlenik { get; set; }
    }

}
