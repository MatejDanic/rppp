﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Cjenik
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int IdNaplatnePostaje { get; set; }
        public string Kategorija { get; set; }
        public string MjestoUlaska { get; set; }
        public decimal Iznos { get; set; }

        public NaplatnaPostaja IdNaplatnePostajeNavigation { get; set; }
    }
}
