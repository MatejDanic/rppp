﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Models
{
    public class UredajPdf
    {
        public int Id { get; set; }
        public int IdObjekta { get; set; }
        public string Naziv { get; set; }
        public int? IdVrsteUredaja { get; set; }

        public Objekt IdObjektaNavigation { get; set; }
        public VrstaUredaja IdVrsteUredajaNavigation { get; set; }
        public ICollection<Incident> Incident { get; set; }
    }
}
