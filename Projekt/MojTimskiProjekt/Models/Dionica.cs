﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Dionica
    {
        public Dionica()
        {
            NaplatnaPostaja = new HashSet<NaplatnaPostaja>();
            Objekt = new HashSet<Objekt>();
            Operater = new HashSet<Operater>();
            ProslaStanja = new HashSet<ProslaStanja>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Naziv je obavezan")]
        public string Naziv { get; set; }
        [Required(ErrorMessage = "Duljina je obavezna")]
        public double Duljina { get; set; }
        [Required(ErrorMessage = "Početak je obavezan")]
        public string Pocetak { get; set; }
        [Required(ErrorMessage = "Kraj je obavezan")]
        public string Kraj { get; set; }
        [Required(ErrorMessage = "IdOperatera je obavezan")]
        public int IdOperatera { get; set; }
        [Required(ErrorMessage = "Stanje je obavezno")]
        public string Stanje { get; set; }
        [Required(ErrorMessage = "Autocesta je obavezna")]
        public int IdAutoceste { get; set; }

       
        public Autocesta IdAutocesteNavigation { get; set; }
        public ICollection<NaplatnaPostaja> NaplatnaPostaja { get; set; }
        public ICollection<Objekt> Objekt { get; set; }
        public ICollection<Operater> Operater { get; set; }
        public ICollection<ProslaStanja> ProslaStanja { get; set; }
    }
}
