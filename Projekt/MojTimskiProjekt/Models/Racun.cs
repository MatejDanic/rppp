﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Racun
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime DatumUlaz { get; set; }
        public DateTime DatumIzlaz { get; set; }
        public int IdKategorijeVozila { get; set; }
        public string RegistracijskeOznake { get; set; }
        public decimal Iznos { get; set; }
        public int IdNacinaPlacanja { get; set; }
        public int IdBlagajnika { get; set; }
        public int IdNaplatneKucice { get; set; }
        public string MjestoUlaska { get; set; }
        public string MjestoIzlaska { get; set; }

        public Blagajnik IdBlagajnikaNavigation { get; set; }
        public KategorijaVozila IdKategorijeVozilaNavigation { get; set; }
        public NacinPlacanja IdNacinaPlacanjaNavigation { get; set; }
        public NaplatnaKucica IdNaplatneKuciceNavigation { get; set; }
    }
}
