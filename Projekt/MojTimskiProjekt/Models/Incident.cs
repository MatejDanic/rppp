﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Incident
    {
        public Incident()
        {
            Alarm = new HashSet<Alarm>();
            KategorijaIncidenta = new HashSet<KategorijaIncidenta>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Naziv je obavezan")]
        public string Naziv { get; set; }
        [Required(ErrorMessage = "uredaj je obavezan")]
        public int IdUredaja { get; set; }
        [Required(ErrorMessage = "Lokacija je obavezan")]
        public string Lokacija { get; set; }
        [Required(ErrorMessage = "Opis je obavezan")]
        public string Opis { get; set; }

        public Uredaj IdUredajaNavigation { get; set; }
        public ICollection<Alarm> Alarm { get; set; }
        public ICollection<KategorijaIncidenta> KategorijaIncidenta { get; set; }
    }
}
