﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MojTimskiProjekt.Models
{
    public class ZaposlenikPdf
    {
        public int Id { get; set; }

        public String Upravitelj { get; set; }

        public String VrstaPosla { get; set; }

        public string Ime { get; set; }

        public string Prezime { get; set; }

        public string Adresa { get; set; }

        public string Oib { get; set; }

        public Upravitelj IdUpraviteljaNavigation { get; set; }
        public ICollection<Blagajnik> Blagajnik { get; set; }
        public ICollection<Operater> Operater { get; set; }
        public VrstaPosla IdVrstePoslaNavigation { get; set; }
    }
}
