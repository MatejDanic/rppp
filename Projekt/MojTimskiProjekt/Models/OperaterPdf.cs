﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class OperaterPdf
    {
        public int Id { get; set; }
        public int IdDionice { get; set; }
        public int? IdZaposlenika { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string OIB { get; set; }
        public string Dionica { get; set; }

        public Dionica IdDioniceNavigation { get; set; }
        public Zaposlenik IdZaposlenikaNavigation { get; set; }
    }
}
