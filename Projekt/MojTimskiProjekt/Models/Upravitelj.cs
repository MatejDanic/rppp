﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Upravitelj
    {
        public Upravitelj()
        {
            Autocesta = new HashSet<Autocesta>();
            Obavijest = new HashSet<Obavijest>();
            Scenarij = new HashSet<Scenarij>();
            Zaposlenik = new HashSet<Zaposlenik>();
        }
        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Naziv je obavezan")]
        public string Naziv { get; set; }
        [Required(ErrorMessage = "Sjedište je obavezno")]
        public string Sjediste { get; set; }
        [Required(ErrorMessage = "OIB je Obavezan")]
        public int Oib { get; set; }
        [EmailAddress(ErrorMessage = "Neispravna e-mail adresa")]
        public string EMail { get; set; }
        [Required(ErrorMessage = "Kontakt je obavezan")]
        public int Kontakt { get; set; }

        public ICollection<Autocesta> Autocesta { get; set; }
        public ICollection<Obavijest> Obavijest { get; set; }
        public ICollection<Scenarij> Scenarij { get; set; }
        public ICollection<Zaposlenik> Zaposlenik { get; set; }
    }
}
