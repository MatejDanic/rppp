﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class Objekt
    {
        public Objekt()
        {
            Uredaj = new HashSet<Uredaj>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    

        [Required(ErrorMessage = "Naziv je obvezno polje")]
        [Display(Name = "Naziv objekta", Prompt ="Unesite naziv objekta")]
        public string Naziv { get; set; }

        [Required(ErrorMessage = "Lokacija je obvezno polje")]
        [Display(Name = "Lokacija objekta", Prompt = "Unesite lokaciju objekta")]
        public string Lokacija { get; set; }

        
        public int IdDionice { get; set; }
        public int? IdVrsteObjekta { get; set; }

        public Dionica IdDioniceNavigation { get; set; }
        public VrstaObjekta IdVrsteObjektaNavigation { get; set; }
        public ICollection<Uredaj> Uredaj { get; set; }
    }
}
