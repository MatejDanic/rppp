﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class NaplatnaPostaja
    {
        public NaplatnaPostaja()
        {
            Cjenik = new HashSet<Cjenik>();
            NaplatnaKucica = new HashSet<NaplatnaKucica>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Lokacija { get; set; }
        public int IdDionice { get; set; }
        public string Vrsta { get; set; }

        public Dionica IdDioniceNavigation { get; set; }
        public ICollection<Cjenik> Cjenik { get; set; }
        public ICollection<NaplatnaKucica> NaplatnaKucica { get; set; }
    }
}
