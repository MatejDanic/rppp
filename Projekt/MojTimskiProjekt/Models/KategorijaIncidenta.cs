﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MojTimskiProjekt.Models
{
    public partial class KategorijaIncidenta
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Kategorija { get; set; }
        public int IdIncidenta { get; set; }

        public Incident IdIncidentaNavigation { get; set; }
    }
}
