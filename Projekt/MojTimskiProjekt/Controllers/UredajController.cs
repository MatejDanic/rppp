﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.ViewModels;

namespace MojTimskiProjekt.Controllers
{
    public class UredajController : Controller
    {

        private readonly RPPP14Context _context;
        private readonly AppSettings appData;
        private readonly ILogger logger;

        public UredajController(RPPP14Context context, IOptionsSnapshot<AppSettings> options, ILogger<UredajController> logger)
        {
            _context = context;
            appData = options.Value;
            this.logger = logger;
        }
        // GET: Uredaj
        public async Task<IActionResult> Index()
        {
            var rPPP14Context = _context.Uredaj
                                     .Include(a => a.IdObjektaNavigation);
            var uredaji = _context.Uredaj
                                    .OrderBy(d => d.Naziv)
                                    .Select(d => new { d.Naziv, d.Id })
                                    .ToList();
            ViewData["Uredaji"] = uredaji;
            ViewBag.IdDionice = new SelectList(uredaji, nameof(Uredaj.IdObjekta), nameof(Uredaj.IdVrsteUredaja)); // MOZDA TRIBA VRATIT NAVIGATION NA KRAJ OVOG UNUTAR NAMEOF
            return View(await rPPP14Context.ToListAsync());
        }


        public IActionResult IndexSimple()
        {
            var uredaji = _context.Uredaj
                .Include(a => a.IdObjektaNavigation)
                .AsNoTracking()
                .Select(m => new UredajViewModel
                {
                    Id = m.Id,
                    Naziv = m.Naziv,
                    // IdObjekta = m.IdObjekta,  MOZDA OVO TRIBA VRATIT
                    IdVrsteUredaja = m.IdVrsteUredaja
                })
                .ToList();
            var model = new UredajiViewModel
            {
                Uredaji = uredaji
            };
            ViewData["uredaji"] = uredaji;
            logger.LogInformation("Učitavanje podataka o uredajima");
            return View(model);
        }

        public IActionResult IndexSimple2()
        {
            var objekti = _context.Objekt
                .AsNoTracking()
                .Include(a => a.IdDioniceNavigation)
                .Include(a => a.IdVrsteObjektaNavigation)
                .OrderBy(a => a.Id)
                .ToList();
            var uredaji = _context.Uredaj
                    .AsNoTracking()
                    .OrderBy(a => a.Naziv)
                    .ToList();

            ViewData["uredaji"] = uredaji;
            return View(objekti);
        }

        // GET: Objekt/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uredaj = await _context.Uredaj
                    .Include(a => a.IdObjektaNavigation)
                    .FirstOrDefaultAsync(m => m.Id == id);
            if (uredaj == null)
            {
                return NotFound();
            }

            return View(uredaj);
        }


        // GET: Autocesta/Create
        public IActionResult Create()
        {
            var uredaji = _context.Uredaj
                                    .OrderBy(d => d.Naziv)
                                    .Select(d => new { d.Naziv, d.Id })
                                    .ToList();
            ViewData["Uredaji"] = uredaji;
            ViewBag.IdDionice = new SelectList(uredaji, nameof(Uredaj.IdObjekta), nameof(Uredaj.IdVrsteUredaja));
            ViewData["IdObjekta"] = new SelectList(_context.Uredaj, "Id", "Naziv");
            ViewData["IdVrsteUredaja"] = new SelectList(_context.VrstaUredaja, "Id", "Vrsta");
            ViewData["IdUredaja"] = new SelectList(_context.Uredaj, "Id", "Naziv");
            return View();
        }

        // POST: Uredaj/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,IdVrsteUredaja,IdObjekta")] Uredaj uredaj)
        {
            logger.LogTrace(JsonConvert.SerializeObject(uredaj));
            if (ModelState.IsValid && !UredajExists(uredaj.Id))
            {
                try
                {
                    _context.Add(uredaj);
                    await _context.SaveChangesAsync();
                    ViewData["message"] = $"Uredaj {uredaj.Naziv} je dodan u bazu";
                    logger.LogInformation(new EventId(1000), $"Uredaj {uredaj.Naziv} je dodan u bazu");
                    return RedirectToAction(nameof(Index), ViewData["message"]);
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom dodavanje novog uredaja: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(uredaj);
                }
            }
            else
            {
                ViewData["IdObjekta"] = new SelectList(_context.Uredaj, "Id", "Naziv");
                ViewData["IdVrsteUredaja"] = new SelectList(_context.VrstaUredaja, "Id", "Vrsta");
                return View(uredaj);
            }

        }

        // GET: Uredaj/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uredaj = await _context.Uredaj.FindAsync(id);
            if (uredaj == null)
            {
                return NotFound();
            }
            ViewData["IdObjekta"] = new SelectList(_context.Uredaj, "Id", "Naziv");
            ViewData["IdVrsteUredaja"] = new SelectList(_context.VrstaUredaja, "Id", "Vrsta");
            return View(uredaj);
        }

        // POST: Uredaj/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,IdVrsteUredaja,IdObjekta")] Uredaj uredaj)
        {
            if (id != uredaj.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(uredaj);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UredajExists(uredaj.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { objektId = uredaj.IdObjekta }); // MOZDA TRIBA MAKNIT NEW DIO
            }
            ViewData["IdObjekta"] = new SelectList(_context.Uredaj, "Id", "Naziv");
            ViewData["IdVrsteUredaja"] = new SelectList(_context.VrstaUredaja, "Id", "Vrsta");
            return View(uredaj);
        }

        // GET: Uredaj/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uredaj = await _context.Uredaj
               .Include(a => a.IdObjektaNavigation)
               .FirstOrDefaultAsync(m => m.Id == id);
            if (uredaj == null)
            {
                return NotFound();
            }

            return View(uredaj);
        }
        

        // POST: Uredaj/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var uredaj = await _context.Uredaj.FindAsync(id);
            if (uredaj != null)
            {
                try
                {
                    _context.Uredaj.Remove(uredaj);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Uredaj {uredaj.Naziv} uspješno obrisan");
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom brisanja uredaja: " + exc.CompleteExceptionMessage());
                }
            }
            else
            {
                logger.LogWarning("Ne postoji uredaj s id-em: {0} ", id);
            }
            return RedirectToAction(nameof(Index), new { objektId = uredaj.IdObjekta }); // mozda triba maknit ovaj new dio
        }



        private bool UredajExists(int id)
        {
            return _context.Uredaj.Any(e => e.Id == id);
        }


        [HttpGet]
        public IActionResult EditPartial(int id)
        {
            var uredaj = _context.Uredaj
                             .AsNoTracking()
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (uredaj != null)
            {

                return PartialView(uredaj);
            }
            else
            {
                return NotFound($"Neispravan id uredaja: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditPartial(Uredaj uredaj)
        {
            if (uredaj == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = _context.Uredaj.Any(m => m.Id == uredaj.Id);
            if (!checkId)
            {
                return NotFound($"Neispravan id uredaja: {uredaj.Id}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(uredaj);
                    _context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Uredaj {uredaj.Naziv} je ažuriran");
                    return StatusCode(302, Url.Action(nameof(Row), new { id = uredaj.Id }));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return PartialView(uredaj);
                }
            }
            else
            {
                return PartialView(uredaj);
            }
        }


        public PartialViewResult Row(int id)
        {
            var uredaj = _context.Uredaj
                             .Where(m => m.Id == id)
                             .Select(m => new UredajViewModel
                             {
                                 Id = m.Id,
                                 Naziv = m.Naziv,
                                 IdObjekta = m.IdObjekta,
                                 IdVrsteUredaja = m.IdVrsteUredaja
                             })
                             .SingleOrDefault();
            if (uredaj != null)
            {
                return PartialView(uredaj);
            }
            else
            {
                //vratiti prazan sadržaj?
                return PartialView("ErrorMessageRow", $"Neispravan id uredaja: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePartial(int id)
        {
            var uredaj = _context.Uredaj
                             .AsNoTracking() //ima utjecaj samo za Update, za brisanje možemo staviti AsNoTracking
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (uredaj != null)
            {
                try
                {
                    string naziv = uredaj.Naziv;
                    _context.Remove(uredaj);
                    _context.SaveChanges();
                    var result = new
                    {
                        message = $"Uredaj {naziv} sa šifrom {id} obrisan.",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = "Pogreška prilikom brisanja uredaja:  " + exc.CompleteExceptionMessage(),
                        successful = false
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Mjesto sa šifrom {id} ne postoji");
            }
        }


    }
}