﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.Controllers
{
    public class DionicaController : Controller
    {
        private readonly RPPP14Context _context;
        private readonly ILogger logger;

        public DionicaController(RPPP14Context context, ILogger<AutocestaController> logger)
        {
            _context = context;
            this.logger = logger;
        }

        // GET: Dionica
        public async Task<IActionResult> Index()
        {
            var rPPP14Context = _context.Dionica.Include(d => d.IdAutocesteNavigation);
            return View(await rPPP14Context.ToListAsync());
        }

        // GET: Dionica/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dionica = await _context.Dionica
                .Include(d => d.IdAutocesteNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dionica == null)
            {
                return NotFound();
            }

            return View(dionica);
        }

        // GET: Dionica/Create
        public IActionResult Create()
        {
            ViewData["IdAutoceste"] = new SelectList(_context.Autocesta, "Id", "Naziv");
            return View();
        }

        // POST: Dionica/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Duljina,Pocetak,Kraj,IdOperatera,Stanje,IdAutoceste")] Dionica dionica)
        {
            if (ModelState.IsValid && !DionicaExists(dionica.Id))
            {
                try
                {
                    _context.Add(dionica);
                    await _context.SaveChangesAsync();
                    logger.LogInformation(new EventId(1000), $"Dionica {dionica.Naziv} je dodana u bazu");
                    return RedirectToAction(nameof(Index));
                }
                catch(Exception exc)
                {

                    logger.LogError("Pogreška prilikom dodavanje nove dionice: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(dionica);
                }
            }
            ViewData["IdAutoceste"] = new SelectList(_context.Autocesta, "Id", "Naziv", dionica.IdAutoceste);
            return View(dionica);
        }

        // GET: Dionica/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dionica = await _context.Dionica.FindAsync(id);
            if (dionica == null)
            {
                return NotFound();
            }
            ViewData["IdAutoceste"] = new SelectList(_context.Autocesta, "Id", "Kraj", dionica.IdAutoceste);
            return View(dionica);
        }

        // POST: Dionica/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,Duljina,Pocetak,Kraj,IdOperatera,Stanje,IdAutoceste")] Dionica dionica)
        {
            if (id != dionica.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(dionica);
                    await _context.SaveChangesAsync();
                    logger.LogInformation(new EventId(1000), $"Dionica {dionica.Naziv} je uspješno ažurirana");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DionicaExists(dionica.Id))
                    {

                        logger.LogError("Pogreška prilikom ažuriranja dionice: {0}");
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAutoceste"] = new SelectList(_context.Autocesta, "Id", "Kraj", dionica.IdAutoceste);
            return View(dionica);
        }

        // GET: Dionica/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var dionica = await _context.Dionica
                .Include(d => d.IdAutocesteNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (dionica == null)
            {
                return NotFound();
            }

            return View(dionica);
        }

        // POST: Dionica/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var dionica = await _context.Dionica.FindAsync(id);
            _context.Dionica.Remove(dionica);
            await _context.SaveChangesAsync();
            logger.LogInformation(new EventId(1000), $"Dionica {dionica.Naziv} je uspješno obrisana");
            return RedirectToAction(nameof(Index));
        }

        private bool DionicaExists(int id)
        {
            return _context.Dionica.Any(e => e.Id == id);
        }
    }
}
