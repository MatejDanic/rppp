﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;
using Newtonsoft.Json;

namespace MojTimskiProjekt.Controllers
{
    public class ProslaStanjaController : Controller
    {
        private readonly RPPP14Context _context;
        private readonly AppSettings appData;
        private readonly ILogger logger;

        public ProslaStanjaController(RPPP14Context context, IOptionsSnapshot<AppSettings> options, ILogger<UpraviteljController> logger)
        {
            _context = context;
            appData = options.Value;
            this.logger = logger;
        }

        // GET: ProslaStanja
        public async Task<IActionResult> Index()
        {
            var rPPP14Context = _context.ProslaStanja.Include(d => d.IdAutocesteNavigation).Include(p => p.IdDioniceNavigation);
            return View(await rPPP14Context.ToListAsync());
        }

        // GET: ProslaStanja/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proslaStanja = await _context.ProslaStanja
                .Include(p => p.IdAutocesteNavigation)
                .Include(p => p.IdDioniceNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (proslaStanja == null)
            {
                return NotFound();
            }

            return View(proslaStanja);
        }

        // GET: ProslaStanja/Create
        public IActionResult Create()
        {
            ViewData["IdAutoceste"] = new SelectList(_context.Autocesta, "Id", "Kraj");
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Kraj");
            return View();
        }

        // POST: ProslaStanja/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DatumOd,DatumDo,Opis,IdAutoceste,IdDionice")] ProslaStanja proslaStanja)
        {
            logger.LogTrace(JsonConvert.SerializeObject(proslaStanja));
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Add(proslaStanja);
                    await _context.SaveChangesAsync();
                    ViewData["message"] = $"Stanje {proslaStanja.Opis} je dodano u bazu";
                    logger.LogInformation(new EventId(1000), $"Stanje {proslaStanja.Opis} je dodano u bazu");
                    return RedirectToAction(nameof(Index), ViewData["message"]);
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom dodavanje novog stanja: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(proslaStanja);
                }
            }
            ViewData["IdAutoceste"] = new SelectList(_context.Autocesta, "Id", "Kraj", proslaStanja.IdAutoceste);
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Kraj", proslaStanja.IdDionice);
            return View(proslaStanja);
        }

        // GET: ProslaStanja/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proslaStanja = await _context.ProslaStanja.FindAsync(id);
            if (proslaStanja == null)
            {
                return NotFound();
            }
            ViewData["IdAutoceste"] = new SelectList(_context.Autocesta, "Id", "Kraj", proslaStanja.IdAutoceste);
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Kraj", proslaStanja.IdDionice);
            return View(proslaStanja);
        }

        // POST: ProslaStanja/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DatumOd,DatumDo,Opis,IdAutoceste,IdDionice")] ProslaStanja proslaStanja)
        {
            if (id != proslaStanja.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(proslaStanja);
                    await _context.SaveChangesAsync();
                    logger.LogInformation(new EventId(1000), $"Stanje {proslaStanja.Opis} je uspješno ažurirano");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProslaStanjaExists(proslaStanja.Id))
                    {
                        logger.LogError("Pogreška prilikom ažuriranja stanja: {0}");
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAutoceste"] = new SelectList(_context.Autocesta, "Id", "Kraj", proslaStanja.IdAutoceste);
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Kraj", proslaStanja.IdDionice);
            return View(proslaStanja);
        }

        // GET: ProslaStanja/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var proslaStanja = await _context.ProslaStanja
                .Include(p => p.IdAutocesteNavigation)
                .Include(p => p.IdDioniceNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (proslaStanja == null)
            {
                return NotFound();
            }

            return View(proslaStanja);
        }

        // POST: ProslaStanja/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var proslaStanja = await _context.ProslaStanja.FindAsync(id);
            _context.ProslaStanja.Remove(proslaStanja);
            await _context.SaveChangesAsync();
            logger.LogInformation(new EventId(1000), $"Stanje {proslaStanja.Opis} je uspješno obrisano");
            return RedirectToAction(nameof(Index));
        }

        private bool ProslaStanjaExists(int id)
        {
            return _context.ProslaStanja.Any(e => e.Id == id);
        }
    }
}
