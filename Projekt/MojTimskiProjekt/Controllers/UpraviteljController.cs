﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.ViewModels;
using Newtonsoft.Json;

namespace MojTimskiProjekt.Controllers
{
    public class UpraviteljController : Controller
    {
        private readonly RPPP14Context _context;
        private readonly AppSettings appData;
        private readonly ILogger logger;

        public UpraviteljController(RPPP14Context context, IOptionsSnapshot<AppSettings> options, ILogger<UpraviteljController> logger)
        {
            _context = context;
            appData = options.Value;
            this.logger = logger;
        }

        // GET: Upravitelj
        public IActionResult Index()
        {
            var upravitelji = _context.Upravitelj.AsNoTracking().OrderBy(a => a.Id).ToList();
            return View("Index", upravitelji);
        }

        public IActionResult IndexSimple()
        {
            var upravitelji = _context.Upravitelj
                .AsNoTracking()
                .Select(m => new UpraviteljViewModel
                {
                    Id = m.Id,
                    Naziv = m.Naziv,
                    Sjediste = m.Sjediste,
                    Oib = m.Oib.ToString(),
                    EMail = m.EMail,
                    Kontakt = m.Kontakt.ToString()
                })
                .ToList();
            var model = new UpraviteljiViewModel
            {
                Upravitelji = upravitelji
            };
            var autoceste = _context.Autocesta
                   .AsNoTracking()
                   .ToList();
            ViewData["autoceste"] = autoceste;
            logger.LogInformation("Učitavanje podataka o upraviteljima");
            return View(model);
        }


        public IActionResult IndexSimple2()
        {
            var upravitelji = _context.Upravitelj
                .AsNoTracking()
                .OrderBy(a => a.Id)
                .ToList();
            var autoceste = _context.Autocesta
                    .AsNoTracking()
                    .OrderBy(a => a.Naziv)
                    .ToList();

            ViewData["autoceste"] = autoceste;
            return View(upravitelji);
        }

        // GET: Upravitelj/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var upravitelj = await _context.Upravitelj
                .FirstOrDefaultAsync(m => m.Id == id);
            if (upravitelj == null)
            {
                return NotFound();
            }

            return View(upravitelj);
        }

        // GET: Upravitelj/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Upravitelj/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Sjediste,Oib,EMail,Kontakt")] Upravitelj upravitelj)
        {
            logger.LogTrace(JsonConvert.SerializeObject(upravitelj));
            if (ModelState.IsValid && !UpraviteljExists(upravitelj.Id))
            {
                try
                {
                    _context.Add(upravitelj);
                    await _context.SaveChangesAsync();
                    ViewData["message"] = $"Upravitelj {upravitelj.Naziv} je dodan u bazu";
                    logger.LogInformation(new EventId(1000), $"Upravitelj {upravitelj.Naziv} je dodan u bazu");
                    return RedirectToAction(nameof(Index), ViewData["message"]);
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom dodavanje novog upravitelja: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(upravitelj);
                }
            }
            return View(upravitelj);
        }

        // GET: Upravitelj/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var upravitelj = await _context.Upravitelj.FindAsync(id);
            if (upravitelj == null)
            {
                return NotFound();
            }
            return View(upravitelj);
        }

        // POST: Upravitelj/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,Sjediste,Oib,EMail,Kontakt")] Upravitelj upravitelj)
        {
            if (id != upravitelj.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(upravitelj);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Upravitelj {upravitelj.Naziv} je ažuriran");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UpraviteljExists(upravitelj.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(upravitelj);
        }

        // GET: Upravitelj/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var upravitelj = await _context.Upravitelj
                .FirstOrDefaultAsync(m => m.Id == id);
            if (upravitelj == null)
            {
                return NotFound();
            }

            return View(upravitelj);
        }

        // POST: Upravitelj/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var upravitelj = await _context.Upravitelj.FindAsync(id);
            if (upravitelj != null)
            {
                try
                {
                    _context.Upravitelj.Remove(upravitelj);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Upravitelj {upravitelj.Naziv} uspješno obrisan");
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom brisanja upravitelja: " + exc.CompleteExceptionMessage());
                }
            }
            else
            {
                logger.LogWarning("Ne postoji upravitelj s id-em: {0} ", id);
            }
            return RedirectToAction(nameof(Index));
        }

        private bool UpraviteljExists(int id)
        {
            return _context.Upravitelj.Any(e => e.Id == id);
        }

        [HttpGet]
        public IActionResult EditPartial(int id)
        {
            var upravitelj = _context.Upravitelj
                             .AsNoTracking()
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (upravitelj != null)
            {

                return PartialView(upravitelj);
            }
            else
            {
                return NotFound($"Neispravan id upravitelja: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditPartial(Upravitelj upravitelj)
        {
            if (upravitelj == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = _context.Zaposlenik.Any(m => m.Id == upravitelj.Id);
            if (!checkId)
            {
                return NotFound($"Neispravan id upravitelja: {upravitelj.Id}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(upravitelj);
                    _context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Upravitelj {upravitelj.Naziv} je ažuriran");
                    return StatusCode(302, Url.Action(nameof(Row), new { id = upravitelj.Id }));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return PartialView(upravitelj);
                }
            }
            else
            {
                return PartialView(upravitelj);
            }
        }

        public PartialViewResult Row(int id)
        {
            var upravitelj = _context.Upravitelj
                             .Where(m => m.Id == id)
                             .Select(m => new UpraviteljViewModel
                             {
                                 Id = m.Id,
                                 Naziv = m.Naziv,
                                 Sjediste = m.Sjediste,
                                 Oib = m.Oib.ToString(),
                                 EMail = m.EMail,
                                 Kontakt = m.Kontakt.ToString()
                             })
                             .SingleOrDefault();
            if (upravitelj != null)
            {
                return PartialView(upravitelj);
            }
            else
            {
                //vratiti prazan sadržaj?
                return PartialView("ErrorMessageRow", $" Neispravan id upravitelja: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePartial(int id)
        {
            var upravitelj = _context.Upravitelj
                             .AsNoTracking() //ima utjecaj samo za Update, za brisanje možemo staviti AsNoTracking
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (upravitelj != null)
            {
                try
                {
                    string naziv = upravitelj.Naziv;
                    _context.Remove(upravitelj);
                    _context.SaveChanges();
                    logger.LogInformation($"Upravitelj {naziv} uspješno obrisan");
                    var result = new
                    {
                        message = $"Upravitelj {naziv} sa šifrom {id} obrisano.",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = "Pogreška prilikom brisanja upravitelja:  " + exc.CompleteExceptionMessage(),
                        successful = false
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Upravitelj sa šifrom {id} ne postoji");
            }
        }

    }
}