﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.ViewModels;


namespace MojTimskiProjekt.Controllers
{
    public class AlarmController : Controller
    {
        private readonly RPPP14Context _context;
        private readonly ILogger logger;

        public AlarmController(RPPP14Context context, ILogger<AutocestaController> logger)
        {
            _context = context;
            this.logger = logger;
        }

        // GET: Alarm
        public async Task<IActionResult> Index()
        {
            var rPPP14Context = _context.Alarm.Include(a => a.IdIncidentaNavigation);
            return View(await rPPP14Context.ToListAsync());
        }


        public IActionResult IndexSimple()
        {
            var alarmi = _context.Alarm
                .AsNoTracking()
                .Include (a => a.IdIncidentaNavigation)
                .Select(m => new AlarmViewModel
                {
                    Id = m.Id,
                    Naziv = m.Naziv,
                    Vrsta = m.Vrsta,
                    IdIncidenta = m.IdIncidenta,
                    LokacijaIncidenta = m.IdIncidentaNavigation.Lokacija
                })
                .ToList();
            var model = new AlarmiViewModel
            {
                Alarmi = alarmi
            };

            
            return View(model);
        }



        // GET: Alarm/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alarm = await _context.Alarm
                .Include(a => a.IdIncidentaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (alarm == null)
            {
                return NotFound();
            }

            return View(alarm);
        }

        // GET: Alarm/Create
        public IActionResult Create()
        {
            ViewData["IdIncidenta"] = new SelectList(_context.Incident, "Id", "Naziv");
            return View();
        }

        // POST: Alarm/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdIncidenta,Naziv,Vrsta")] Alarm alarm)
        {
            if (ModelState.IsValid)
            {
                _context.Add(alarm);
                await _context.SaveChangesAsync();
                logger.LogInformation(new EventId(1000), $"Alarm s id-em {alarm.Id} je dodan u bazu");
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdIncidenta"] = new SelectList(_context.Incident, "Id", "Naziv", alarm.IdIncidenta);
            return View(alarm);
        }

        // GET: Alarm/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alarm = await _context.Alarm.FindAsync(id);
            if (alarm == null)
            {
                return NotFound();
            }
            ViewData["IdIncidenta"] = new SelectList(_context.Incident, "Id", "Lokacija", alarm.IdIncidenta);
            return View(alarm);
        }

        // POST: Alarm/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdIncidenta,Naziv,Vrsta")] Alarm alarm)
        {
            if (id != alarm.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(alarm);
                    await _context.SaveChangesAsync();
                    logger.LogInformation(new EventId(1000), $"Alarm s id-em {alarm.Id} je ažuriran");
                }
                catch (DbUpdateConcurrencyException)
                {
                    logger.LogError("Pogreška prilikom ažuriranja alarma");
                    if (!AlarmExists(alarm.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdIncidenta"] = new SelectList(_context.Incident, "Id", "Lokacija", alarm.IdIncidenta);
            return View(alarm);
        }


        [HttpGet]
        public IActionResult EditPartial(int id)
        {
            var alarm = _context.Alarm
                             .AsNoTracking()
                             .Include(m => m.IdIncidentaNavigation)
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (alarm != null)
            {
               
                return PartialView(alarm);
            }
            else
            {
                return NotFound($"Neispravan id alarma: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditPartial(Alarm alarm)
        {
            if (alarm == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = _context.Alarm.Any(m => m.Id == alarm.Id);
            if (!checkId)
            {
                return NotFound($"Neispravan id alarma: {alarm?.Id}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                   _context.Update(alarm);
                   _context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Alarm s id-em {alarm.Id} je ažuriran");
                    return StatusCode(302, Url.Action(nameof(Row), new { id = alarm.Id }));
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom ažuriranja alarma");
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return PartialView(alarm);
                }
            }
            else
            {
                return PartialView(alarm);
            }
        }


        public PartialViewResult Row(int id)
        {
            var alarm = _context.Alarm
                             .Where(m => m.Id == id)
                             .Include (m => m.IdIncidentaNavigation)
                             .Select(m => new AlarmViewModel
                             {
                                 Id = m.Id,
                                 Naziv = m.Naziv,
                                 Vrsta = m.Vrsta,
                                 IdIncidenta = m.IdIncidenta,
                                 LokacijaIncidenta = m.IdIncidentaNavigation.Lokacija
                             })
                             .SingleOrDefault();
            if (alarm != null)
            {
                return PartialView(alarm);
            }
            else
            {
                //vratiti prazan sadržaj?
                return PartialView("ErrorMessageRow", $"Neispravan id alarma: {id}");
            }
        }


        // GET: Alarm/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alarm = await _context.Alarm
                .Include(a => a.IdIncidentaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (alarm == null)
            {
                return NotFound();
            }

            return View(alarm);
        }

        // POST: Alarm/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var alarm = await _context.Alarm.FindAsync(id);
            _context.Alarm.Remove(alarm);
            await _context.SaveChangesAsync();
            logger.LogInformation(new EventId(1000), $"Alarm s id-em {alarm.Id} je obrisan");
            return RedirectToAction(nameof(Index));
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePartial(int id)
        {
            var alarm = _context.Alarm
                             .AsNoTracking() //ima utjecaj samo za Update, za brisanje možemo staviti AsNoTracking
                             .Where(m => m.Id== id)
                             .SingleOrDefault();
            if (alarm != null)
            {
                try
                {
                    string naziv = alarm.Naziv;
                    _context.Remove(alarm);
                    _context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Alarm s id-em {alarm.Id} je obrisan");
                    var result = new
                    {
                        message = $"Alarm {naziv} sa šifrom {id} obrisan.",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom brisanja alarma");
                    var result = new
                    {
                        message = "Pogreška prilikom brisanja alarma: " + exc.CompleteExceptionMessage(),
                        successful = false
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Alarm sa šifrom {id} ne postoji");
            }
        }




        private bool AlarmExists(int id)
        {
            return _context.Alarm.Any(e => e.Id == id);
        }
    }
}
