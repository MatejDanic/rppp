﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.Controllers
{
    public class RacunController : Controller
    {
        private readonly RPPP14Context _context;

        public RacunController(RPPP14Context context)
        {
            _context = context;
        }

        // GET: Racun
        public async Task<IActionResult> Index()
        {
            //var racuni = context.Racuni.AsNoTracking().ToList();
            var rPPP14Context = _context.Racun.Include(r => r.IdBlagajnikaNavigation).Include(r => r.IdKategorijeVozilaNavigation).Include(r => r.IdNacinaPlacanjaNavigation).Include(r => r.IdNaplatneKuciceNavigation);
            return View(await rPPP14Context.ToListAsync());
        }

        // GET: Racun/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var racun = await _context.Racun
                .Include(r => r.IdBlagajnikaNavigation)
                .Include(r => r.IdKategorijeVozilaNavigation)
                .Include(r => r.IdNacinaPlacanjaNavigation)
                .Include(r => r.IdNaplatneKuciceNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (racun == null)
            {
                return NotFound();
            }

            return View(racun);
        }

        // GET: Racun/Create
        public IActionResult Create()
        {
            ViewData["IdBlagajnika"] = new SelectList(_context.Blagajnik, "Id", "Id");
            ViewData["IdKategorijeVozila"] = new SelectList(_context.KategorijaVozila, "Id", "KategorijaVozila1");
            ViewData["IdNacinaPlacanja"] = new SelectList(_context.NacinPlacanja, "Id", "NacinPlacanja1");
            ViewData["IdNaplatneKucice"] = new SelectList(_context.NaplatnaKucica, "Id", "Id");
            return View();
        }

        // POST: Racun/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DatumUlaz,DatumIzlaz,IdKategorijeVozila,RegistracijskeOznake,Iznos,IdNacinaPlacanja,IdBlagajnika,IdNaplatneKucice,MjestoUlaska,MjestoIzlaska")] Racun racun)
        {
            if (ModelState.IsValid)
            {
                _context.Add(racun);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdBlagajnika"] = new SelectList(_context.Blagajnik, "Id", "Prezime", racun.IdBlagajnika);
            ViewData["IdKategorijeVozila"] = new SelectList(_context.KategorijaVozila, "Id", "KategorijaVozila1", racun.IdKategorijeVozila);
            ViewData["IdNacinaPlacanja"] = new SelectList(_context.NacinPlacanja, "Id", "NacinPlacanja1", racun.IdNacinaPlacanja);
            ViewData["IdNaplatneKucice"] = new SelectList(_context.NaplatnaKucica, "Id", "Id", racun.IdNaplatneKucice);
            return View(racun);
        }

        // GET: Racun/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var racun = await _context.Racun.FindAsync(id);
            if (racun == null)
            {
                return NotFound();
            }
            ViewData["IdBlagajnika"] = new SelectList(_context.Blagajnik, "Id", "Id", racun.IdBlagajnika);
            ViewData["IdKategorijeVozila"] = new SelectList(_context.KategorijaVozila, "Id", "KategorijaVozila1", racun.IdKategorijeVozila);
            ViewData["IdNacinaPlacanja"] = new SelectList(_context.NacinPlacanja, "Id", "NacinPlacanja1", racun.IdNacinaPlacanja);
            ViewData["IdNaplatneKucice"] = new SelectList(_context.NaplatnaKucica, "Id", "Id", racun.IdNaplatneKucice);
            return View(racun);
        }

        // POST: Racun/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DatumUlaz,DatumIzlaz,IdKategorijeVozila,RegistracijskeOznake,Iznos,IdNacinaPlacanja,IdBlagajnika,IdNaplatneKucice,MjestoUlaska,MjestoIzlaska")] Racun racun)
        {
            if (id != racun.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(racun);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RacunExists(racun.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdBlagajnika"] = new SelectList(_context.Blagajnik, "Id", "Id", racun.IdBlagajnika);
            ViewData["IdKategorijeVozila"] = new SelectList(_context.KategorijaVozila, "Id", "KategorijaVozila1", racun.IdKategorijeVozila);
            ViewData["IdNacinaPlacanja"] = new SelectList(_context.NacinPlacanja, "Id", "NacinPlacanja1", racun.IdNacinaPlacanja);
            ViewData["IdNaplatneKucice"] = new SelectList(_context.NaplatnaKucica, "Id", "Id", racun.IdNaplatneKucice);
            return View(racun);
        }

        // GET: Racun/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var racun = await _context.Racun
                .Include(r => r.IdBlagajnikaNavigation)
                .Include(r => r.IdKategorijeVozilaNavigation)
                .Include(r => r.IdNacinaPlacanjaNavigation)
                .Include(r => r.IdNaplatneKuciceNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (racun == null)
            {
                return NotFound();
            }

            return View(racun);
        }

        // POST: Racun/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var racun = await _context.Racun.FindAsync(id);
            _context.Racun.Remove(racun);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RacunExists(int id)
        {
            return _context.Racun.Any(e => e.Id == id);
        }
    }
}
