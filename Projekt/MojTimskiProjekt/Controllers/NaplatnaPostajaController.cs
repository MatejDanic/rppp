﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.ViewModels;

namespace MojTimskiProjekt.Controllers
{
    public class NaplatnaPostajaController : Controller
    {

        private readonly RPPP14Context _context;
        private readonly ILogger logger;

        public NaplatnaPostajaController(RPPP14Context context, ILogger<NaplatnaPostajaController> logger)
        {
            _context = context;
            this.logger = logger;
        }

        // GET: NaplatnaPostaja
        public ActionResult Index()
        {
            var naplatnePostaje = _context.NaplatnaPostaja.AsNoTracking().OrderBy(a => a.Id).ToList();
            ViewBag.Message = naplatnePostaje;
            return View();
        }

        // GET: NaplatnaPostaja/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }



        // POST: NaplatnaPostaja/Create
        //[HttpPost]
        //  [ValidateAntiForgeryToken]
        /*
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }*/

        // GET: NaplatnaPostaja/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: NaplatnaPostaja/Edit/5
        /*
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        */
        // GET: NaplatnaPostaja/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var naplatnapostaja = await _context.NaplatnaPostaja
                .Include(a => a.IdDionice)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (naplatnapostaja == null)
            {
                return NotFound();
            }

            return View(naplatnapostaja);
        }

        public ActionResult IndexSimple2()
        {
            var naplatnepostaje = _context.NaplatnaPostaja
                .AsNoTracking()
                .Select(m => new NaplatnaPostajaViewModel
                {
                    Id = m.Id,
                    Lokacija = m.Lokacija,
                    Vrsta = m.Vrsta,
                    IdDionice = m.IdDionice
                })
                .ToList();
            var model = new NaplatnePostajeViewModel
            {
                NaplatnePostaje = naplatnepostaje
            };
            var cjenici = _context.Cjenik
                    .AsNoTracking()
                    .ToList();

            ViewData["cjenici"] = cjenici;

            // logger.LogInformation("Učitavanje podataka o autocestama");
            return View(model);
        }

        // POST: NaplatnaPostaja/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var naplatnapostaja = await _context.NaplatnaPostaja.FindAsync(id);
            if (naplatnapostaja != null)
            {
                try
                {
                    String naziv = naplatnapostaja.Lokacija;
                    _context.NaplatnaPostaja.Remove(naplatnapostaja);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Naplatna postaja {naziv} je uspješno obrisana");
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom brisanja naplatne postaje: " + exc.CompleteExceptionMessage());
                }
            }
            else
            {

                logger.LogWarning("Ne postoji naplatna postaja s id-em: {0} ", id);
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult EditPartial(int id)
        {
            var naplatnapostaja = _context.NaplatnaPostaja
                             .AsNoTracking()
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (naplatnapostaja != null)
            {

                return PartialView(naplatnapostaja);
            }
            else
            {
                return NotFound($"Neispravan id naplatne postaje: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditPartial(NaplatnaPostaja naplatnapostaja)
        {
            if (naplatnapostaja == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = _context.NaplatnaPostaja.Any(m => m.Id == naplatnapostaja.Id);
            if (!checkId)
            {
                return NotFound($"Neispravan id naplatne postaje !: {naplatnapostaja.Id}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(naplatnapostaja);
                    _context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Naplatna postaja {naplatnapostaja.Lokacija} je ažurirana");
                    return StatusCode(302, Url.Action(nameof(Row), new { id = naplatnapostaja.Id }));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return PartialView(naplatnapostaja);
                }
            }
            else
            {
                return PartialView(naplatnapostaja);
            }
        }


        public PartialViewResult Row(int id)
        {
            var naplatnapostaja = _context.NaplatnaPostaja
                             .Where(m => m.Id == id)
                             .Select(m => new NaplatnaPostajaViewModel
                             {
                                 Id = m.Id,
                                 Lokacija = m.Lokacija,
                                 Vrsta = m.Vrsta,
                                 IdDionice = m.IdDionice,

                             })
                             .SingleOrDefault();
            if (naplatnapostaja != null)
            {
                return PartialView(naplatnapostaja);
            }
            else
            {
                return PartialView("ErrorMessageRow", $"Neispravan id naplatne postaje: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePartial(int id)
        {
            var naplatnapostaja = _context.NaplatnaPostaja
                             .AsNoTracking() //ima utjecaj samo za Update, za brisanje možemo staviti AsNoTracking
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (naplatnapostaja != null)
            {
                try
                {
                    string lokacija = naplatnapostaja.Lokacija;
                    _context.Remove(naplatnapostaja);
                    _context.SaveChanges();
                    logger.LogInformation($"Naplatna postaja {lokacija} uspješno obrisana");
                    var result = new
                    {
                        message = $"Naplatna postaja {lokacija} sa šifrom {id} obrisano.",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = "Pogreška prilikom brisanja naplatne postaje:  " + exc.CompleteExceptionMessage(),
                        successful = false
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Naplatna postaja sa šifrom {id} ne postoji");
            }
        }



        // GET: Autocesta/Create
        public IActionResult Create()
        {
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Naziv");
            return View();
        }

        // POST: Autocesta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Create([Bind("Id,Lokacija,IdDionice,Vrsta")] NaplatnaPostaja naplatnapostaja)
        {

            logger.LogTrace(JsonConvert.SerializeObject(naplatnapostaja));
            if (ModelState.IsValid && !NaplatnaPostajaExists(naplatnapostaja.Id))
            {
                try
                {
                    _context.Add(naplatnapostaja);
                    await _context.SaveChangesAsync();
                    ViewData["message"] = $"Naplatna postaja {naplatnapostaja.Lokacija} je dodana u bazu";

                    logger.LogInformation(new EventId(1000), $"Naplatna postaja {naplatnapostaja.Lokacija} je dodana u bazu");
                    return RedirectToAction(nameof(Index), ViewData["message"]);
                }
                catch (Exception exc)
                {

                    logger.LogError("Pogreška prilikom dodavanje nove naplatne postaje: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(naplatnapostaja);
                }

            }

            else
            {
                ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Naziv", naplatnapostaja.IdDionice);
                return View(naplatnapostaja);

            }


        }
        private bool NaplatnaPostajaExists(int id)
        {
            return _context.NaplatnaPostaja.Any(e => e.Id == id);
        }
    }
}