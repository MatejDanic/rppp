﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.ViewModels;

namespace MojTimskiProjekt.Controllers
{


    public class CjenikController : Controller
    {
        private readonly RPPP14Context _context;

        public CjenikController(RPPP14Context context)
        {
            _context = context;
        }

        // GET: Cjenik
        public ActionResult Index(int NaplatnaPostajaId)
        {
            ViewBag.Message = _context.Cjenik.AsNoTracking().Where(m => m.IdNaplatnePostaje == NaplatnaPostajaId).ToList();
            return View();
        }

        // GET: Cjenik/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cjenik/Create
        public ActionResult Create()
        {
            return View();
        }
        /*
        // POST: Cjenik/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Cjenik/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }
        
        // POST: Cjenik/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Cjenik/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Cjenik/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }*/
    }
}