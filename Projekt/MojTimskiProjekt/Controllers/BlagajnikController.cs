﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MojTimskiProjekt.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MojTimskiProjekt.ViewModels;
using MojTimskiProjekt.Extensions;
using Newtonsoft.Json;

namespace MojTimskiProjekt.Controllers
{
    public class BlagajnikController : Controller
    {
        private readonly RPPP14Context _context;
        private readonly AppSettings appData;
        private readonly ILogger logger;

        public BlagajnikController(RPPP14Context context, IOptionsSnapshot<AppSettings> options, ILogger<BlagajnikController> logger)
        {
            _context = context;
            appData = options.Value;
            this.logger = logger;
        }

        // GET: Blagajnik
        public async Task<IActionResult> Index()
        {
            var blagajnici = _context.Blagajnik
                                        .Include(b => b.IdNaplatneKućiceNavigation)
                                        .Include(b => b.IdZaposlenikaNavigation)
                                        .AsNoTracking()
                                        .OrderBy(d => d.Id)
                                        .ToList();
            var racuni = _context.Racun
                    .AsNoTracking()
                    .ToList();

            int count = blagajnici.Count;
            if(count == 0)
            {

                logger.LogInformation("Ne postoji nijedan blagajnik");
            }
            ViewData["racuni"] = racuni;
                return View(blagajnici);
        }

        public IActionResult IndexSimple()
        {
            var blagajnici = _context.Blagajnik
                .Include(z => z.IdZaposlenikaNavigation)
                .Include(z => z.IdNaplatneKućiceNavigation)
                .AsNoTracking()
                .Select(m => new BlagajnikViewModel
                {
                    Id = m.Id,
                    IdZaposlenika = m.IdZaposlenika,
                    IdNaplatneKućice = m.IdNaplatneKućice,
                    IdZaposlenikaNavigation = m.IdZaposlenikaNavigation,
                    IdNaplatneKućiceNavigation = m.IdNaplatneKućiceNavigation
                })
                .ToList();
            var model = new BlagajniciViewModel
            {
                Blagajnici = blagajnici
            };
            var racuni = _context.Racun
                    .AsNoTracking()
                    .ToList();

            ViewData["racuni"] = racuni;

            // logger.LogInformation("Učitavanje podataka o autocestama");
            return View(model);
        }



        [HttpGet]
        public IActionResult EditPartial(int id)
        {
            ViewData["IdNaplatnekućice"] = new SelectList(_context.NaplatnaKucica, "Id", "Naziv");
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime");
            var blagajnik = _context.Blagajnik
                             .Include(z => z.IdZaposlenikaNavigation)
                             .Include(z => z.IdNaplatneKućiceNavigation)
                             .AsNoTracking()
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (blagajnik != null)
            {
                return PartialView(blagajnik);
            }
            else
            {
                return NotFound($"Neispravan id blagajnika: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditPartial(Blagajnik blagajnik)
        {
            if (blagajnik == null)
            {
                return NotFound("Nema poslanih podataka");
            }

            bool checkId = _context.Blagajnik.Any(m => m.Id == blagajnik.Id);
            if (!checkId)
            {
                return NotFound($"Neispravan id blagajnika: {blagajnik.Id}");
            }
            ViewData["IdNaplatnekućice"] = new SelectList(_context.NaplatnaKucica, "Id", "Naziv");
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime");
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(blagajnik);
                    _context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Blagajnik s šifrom {blagajnik.Id} je ažuriran");
                    return StatusCode(302, Url.Action(nameof(Row), new { id = blagajnik.Id }));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return PartialView(blagajnik);
                }
            }
            else
            {
                return PartialView(blagajnik);
            }
        }


        public PartialViewResult Row(int id)
        {
            var blagajnik = _context.Blagajnik
                             .Where(m => m.Id == id)
                             .Select(m => new BlagajnikViewModel
                             {
                                 Id = m.Id,
                                 IdZaposlenika = m.IdZaposlenika,
                                 IdNaplatneKućice = m.IdNaplatneKućice,
                                 IdZaposlenikaNavigation = m.IdZaposlenikaNavigation,
                                 IdNaplatneKućiceNavigation = m.IdNaplatneKućiceNavigation

                             })
                             .SingleOrDefault();
            if (blagajnik != null)
            {
                return PartialView(blagajnik);
            }
            else
            {
                //vratiti prazan sadržaj?
                return PartialView("ErrorMessageRow", $"Neispravan id blagajnika: {id}");
            }
        }
        // GET: Blagajnik/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blagajnik = await _context.Blagajnik
                .Include(b => b.IdNaplatneKućiceNavigation)
                .Include(b => b.IdZaposlenikaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (blagajnik == null)
            {
                return NotFound();
            }

            return View(blagajnik);
        }

        // GET: Blagajnik/Create
        public IActionResult Create()
        {
            ViewData["IdNaplatneKućice"] = new SelectList(_context.NaplatnaKucica, "Id", "Naziv");
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime");
            return View();
        }

        // POST: Blagajnik/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdNaplatneKućice,IdZaposlenika")] Blagajnik blagajnik)
        {
            logger.LogTrace(JsonConvert.SerializeObject(blagajnik));
            if (ModelState.IsValid && !BlagajnikExists(blagajnik.Id))
            {
                
                try
                {
                    _context.Add(blagajnik);
                    await _context.SaveChangesAsync();
                    ViewData["message"] = $"Blagajnik {blagajnik.IdZaposlenikaNavigation.Prezime} je dodan u bazu";

                    logger.LogInformation(new EventId(1000), $"Blagajnik {blagajnik.IdZaposlenikaNavigation.Prezime} je dodan u bazu");
                    return RedirectToAction(nameof(Index), ViewData["message"]);
                } catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom dodavanje novog blagajnika: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(blagajnik);
                }
            }
            ViewData["IdNaplatneKućice"] = new SelectList(_context.NaplatnaKucica, "Id", "Naziv", blagajnik.IdNaplatneKućice);
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime", blagajnik.IdZaposlenika);
            return View(blagajnik);
        }

        // GET: Blagajnik/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blagajnik = await _context.Blagajnik.FindAsync(id);
            if (blagajnik == null)
            {
                return NotFound();
            }
            ViewData["IdNaplatneKućice"] = new SelectList(_context.NaplatnaKucica, "Id", "Naziv", blagajnik.IdNaplatneKućice);
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime", blagajnik.IdZaposlenika);
            return View(blagajnik);
        }

        // POST: Blagajnik/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdNaplatneKućice,IdZaposlenika")] Blagajnik blagajnik)
        {
            if (id != blagajnik.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(blagajnik);
                    await _context.SaveChangesAsync();

                    logger.LogInformation($"Blagajnik {blagajnik.IdZaposlenikaNavigation.Prezime} je ažuriran");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BlagajnikExists(blagajnik.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdNaplatneKućice"] = new SelectList(_context.NaplatnaKucica, "Id", "Naziv", blagajnik.IdNaplatneKućice);
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime", blagajnik.IdZaposlenika);
            return View(blagajnik);
        }

        // GET: Blagajnik/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var blagajnik = await _context.Blagajnik
                .Include(b => b.IdNaplatneKućiceNavigation)
                .Include(b => b.IdZaposlenikaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (blagajnik == null)
            {
                return NotFound();
            }

            return View(blagajnik);
        }

        // POST: Blagajnik/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var blagajnik = await _context.Blagajnik
                .Include(b => b.IdNaplatneKućiceNavigation)
                .Include(b => b.IdZaposlenikaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (blagajnik != null)
            {
                
                try
                {
                    string naziv = blagajnik.IdZaposlenikaNavigation.Prezime;
                    _context.Blagajnik.Remove(blagajnik);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Blagajnik {naziv} uspješno obrisan");
                } catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom brisanja blagajnika: " + exc.CompleteExceptionMessage());
                }
            } else
            {
                logger.LogWarning("Ne postoji blagajnik s id-em: {0} ", id);
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePartial(int id)
        {
            var blagajnik = _context.Blagajnik
                             .Include(b => b.IdNaplatneKućiceNavigation)
                             .Include(b => b.IdZaposlenikaNavigation)
                             .AsNoTracking() //ima utjecaj samo za Update, za brisanje možemo staviti AsNoTracking
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (blagajnik != null)
            {
                try
                {
                    string naziv = blagajnik.IdZaposlenikaNavigation.Prezime;
                    _context.Remove(blagajnik);
                    _context.SaveChanges();
                    logger.LogInformation($"Blagajnik {naziv} uspješno obrisan");
                    var result = new
                    {
                        message = $"Blagajnik {naziv} sa šifrom {id} obrisan.",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = "Pogreška prilikom brisanja blagajnika:  " + exc.CompleteExceptionMessage(),
                        successful = false
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Blagajnik sa šifrom {id} ne postoji");
            }
        }

        private bool BlagajnikExists(int id)
        {
            return _context.Blagajnik.Any(e => e.Id == id);
        }
    }
}
