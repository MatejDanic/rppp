﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.ViewModels;

namespace MojTimskiProjekt.Controllers
{
    public class ZaposlenikController : Controller
    {
        private readonly RPPP14Context _context;
        private readonly ILogger logger;

        public ZaposlenikController(RPPP14Context context, ILogger<ZaposlenikController> logger)
        {
            _context = context;
            this.logger = logger;
        }

        // GET: Zaposlenik
        public async Task<IActionResult> Index()
        {
            var rPPP14Context = _context.Zaposlenik.Include(z => z.IdUpraviteljaNavigation).Include(z => z.IdVrstePoslaNavigation);
            return View(await rPPP14Context.ToListAsync());
        }


        public IActionResult IndexSimple()
        {
            var zaposlenici = _context.Zaposlenik
                .Include(z => z.IdUpraviteljaNavigation)
                .Include(z => z.IdVrstePoslaNavigation)
                .AsNoTracking()
                .Select(m => new ZaposlenikViewModel
                {
                    Id = m.Id,
                    Ime = m.Ime,
                    Prezime = m.Prezime,
                    Adresa = m.Adresa,
                    Oib = m.Oib,
                    IdUpravitelja = m.IdUpraviteljaNavigation.Id,
                    IdVrstePosla = m.IdVrstePoslaNavigation.Id,
                    IdUpraviteljaNavigation = m.IdUpraviteljaNavigation,
                    IdVrstePoslaNavigation = m.IdVrstePoslaNavigation

                })
                .ToList();
            var model = new ZaposleniciViewModel
            {
                Zaposlenici = zaposlenici
            };

            // logger.LogInformation("Učitavanje podataka o autocestama");
            return View(model);
        }

        // GET: Zaposlenik/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var zaposlenik = await _context.Zaposlenik
                .Include(z => z.IdUpraviteljaNavigation)
                .Include(z => z.IdVrstePoslaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (zaposlenik == null)
            {
                return NotFound();
            }

            return View(zaposlenik);
        }

        // GET: Zaposlenik/Create
        public IActionResult Create()
        {
            ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv");
            ViewData["IdVrstePosla"] = new SelectList(_context.VrstaPosla, "Id", "Vrsta");
            return View();
        }

        // POST: Zaposlenik/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdUpravitelja,IdVrstePosla,Ime,Prezime,Adresa,Oib")] Zaposlenik zaposlenik)
        {
            logger.LogTrace(JsonConvert.SerializeObject(zaposlenik));
            if (ModelState.IsValid && !ZaposlenikExists(zaposlenik.Id))
            {
                try
                {
                    _context.Add(zaposlenik);
                    await _context.SaveChangesAsync();
                    ViewData["message"] = $"Zaposlenik {zaposlenik.Prezime} je dodan u bazu";

                    logger.LogInformation(new EventId(1000), $"Zaposlenik {zaposlenik.Prezime} je dodan u bazu");
                    return RedirectToAction(nameof(Index), ViewData["message"]);
                }
                catch (Exception exc)
                {

                    logger.LogError("Pogreška prilikom dodavanje novog zaposlenika: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(zaposlenik);
                }
            }
            ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv", zaposlenik.IdUpravitelja);
            ViewData["IdVrstePosla"] = new SelectList(_context.VrstaPosla, "Id", "Vrsta", zaposlenik.IdVrstePosla);
            return View(zaposlenik);
        }

        // GET: Zaposlenik/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var zaposlenik = await _context.Zaposlenik.FindAsync(id);
            if (zaposlenik == null)
            {
                return NotFound();
            }
            ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv", zaposlenik.IdUpravitelja);
            ViewData["IdVrstePosla"] = new SelectList(_context.VrstaPosla, "Id", "Vrsta", zaposlenik.IdVrstePosla);
            return View(zaposlenik);
        }

        // POST: Zaposlenik/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdUpravitelja,IdVrstePosla,Ime,Prezime,Adresa,Oib")] Zaposlenik zaposlenik)
        {
            if (id != zaposlenik.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(zaposlenik);
                    await _context.SaveChangesAsync();

                    logger.LogInformation($"Zaposlenik {zaposlenik.Prezime} je ažuriran");

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ZaposlenikExists(zaposlenik.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv", zaposlenik.IdUpravitelja);
            ViewData["IdVrstePosla"] = new SelectList(_context.VrstaPosla, "Id", "Vrsta", zaposlenik.IdVrstePosla);
            return View(zaposlenik);
        }

        // GET: Zaposlenik/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var zaposlenik = await _context.Zaposlenik
                .Include(z => z.IdUpraviteljaNavigation)
                .Include(z => z.IdVrstePoslaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (zaposlenik == null)
            {
                return NotFound();
            }

            return View(zaposlenik);
        }

        // POST: Zaposlenik/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var zaposlenik = await _context.Zaposlenik.FindAsync(id);
            if (zaposlenik != null)
            {
                try
                {
                    _context.Zaposlenik.Remove(zaposlenik);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Zaposlenik {zaposlenik.Prezime} uspješno obrisan");
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom brisanja zaposlenika: " + exc.CompleteExceptionMessage());
                }
            } else
            {
                logger.LogWarning("Ne postoji zaposlenik s id-em: {0} ", id);
            }
            return RedirectToAction(nameof(Index));
        }

        



        private bool ZaposlenikExists(int id)
        {
            return _context.Zaposlenik.Any(e => e.Id == id);
        }


        [HttpGet]
        public IActionResult EditPartial(int id)
        {
            var zaposlenik = _context.Zaposlenik
                             .Include(z => z.IdUpraviteljaNavigation)
                             .Include(z => z.IdVrstePoslaNavigation)
                             .AsNoTracking()
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv", zaposlenik.IdUpravitelja);
            ViewData["IdVrstePosla"] = new SelectList(_context.VrstaPosla, "Id", "Vrsta", zaposlenik.IdVrstePosla);
            if (zaposlenik != null)
            {

                return PartialView(zaposlenik);
            }
            else
            {
                return NotFound($"Neispravan id zaposlenika: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditPartial(Zaposlenik zaposlenik)
        {
            if (zaposlenik == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = _context.Zaposlenik.Any(m => m.Id == zaposlenik.Id);
            if (!checkId)
            {
                return NotFound($"Neispravan id zaposlenika: {zaposlenik.Id}");
            }
            ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv", zaposlenik.IdUpravitelja);
            ViewData["IdVrstePosla"] = new SelectList(_context.VrstaPosla, "Id", "Vrsta", zaposlenik.IdVrstePosla);
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(zaposlenik);
                    _context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Zaposlenik {zaposlenik.Prezime} je ažuriran");
                    return StatusCode(302, Url.Action(nameof(Row), new { id = zaposlenik.Id }));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return PartialView(zaposlenik);
                }
            }
            else
            {
                return PartialView(zaposlenik);
            }
        }


        public PartialViewResult Row(int id)
        {
            var zaposlenik = _context.Zaposlenik
                             .Where(m => m.Id == id)
                             .Select(m => new ZaposlenikViewModel
                             {
                                 Id = m.Id,
                                 Ime = m.Ime,
                                 Prezime = m.Prezime,
                                 Adresa = m.Adresa,
                                 Oib = m.Oib,
                                 IdUpravitelja = m.IdUpraviteljaNavigation.Id,
                                 IdVrstePosla = m.IdVrstePoslaNavigation.Id,
                                 IdUpraviteljaNavigation = m.IdUpraviteljaNavigation,
                                 IdVrstePoslaNavigation = m.IdVrstePoslaNavigation
                             })
                             .SingleOrDefault();
            if (zaposlenik != null)
            {
                return PartialView(zaposlenik);
            }
            else
            {
                //vratiti prazan sadržaj?
                return PartialView("ErrorMessageRow", $"Neispravan id zaposlenika: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePartial(int id)
        {
            var zaposlenik = _context.Zaposlenik
                             .AsNoTracking() //ima utjecaj samo za Update, za brisanje možemo staviti AsNoTracking
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (zaposlenik != null)
            {
                try
                {
                    string prezime = zaposlenik.Prezime;
                    _context.Remove(zaposlenik);
                    _context.SaveChanges();
                    logger.LogInformation($"Zaposlenik {prezime} uspješno obrisan");
                    var result = new
                    {
                        message = $"Zaposlenik {prezime} sa šifrom {id} obrisano.",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = "Pogreška prilikom brisanja zaposlenika:  " + exc.CompleteExceptionMessage(),
                        successful = false
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Mjesto sa šifrom {id} ne postoji");
            }
        }
    }

}