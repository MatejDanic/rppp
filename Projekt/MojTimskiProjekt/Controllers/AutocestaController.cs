﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.ViewModels;

namespace MojTimskiProjekt.Controllers
{
    public class AutocestaController : Controller
    {
        private readonly RPPP14Context _context;
  
        private readonly ILogger logger;

        public AutocestaController(RPPP14Context context, ILogger<AutocestaController> logger)
        {
            _context = context;
            this.logger = logger;
        }


        public IActionResult Karta()
        {
            return View();
        }


        // GET: Autocesta
        public async Task<IActionResult> Index()
        {   
            var rPPP14Context = _context.Autocesta.Include(a => a.IdUpraviteljaNavigation);
            var dionice = _context.Dionica
                   .OrderBy(d => d.Naziv)
                   .Select(d => new { d.Naziv, d.IdAutoceste })
                   .ToList();
            ViewData["Dionice"] = dionice;

            
            return View(await rPPP14Context.ToListAsync());
        }
        private void GetDionice()
        {
            var dionice = _context.Dionica
                   .OrderBy(d => d.Naziv)
                   .Select(d => new { d.Naziv, d.IdAutoceste })
                   .ToList();
            ViewBag.Dionice = new SelectList(dionice, nameof(Dionica.Naziv), nameof(Dionica.IdAutoceste));
            ViewData["Dionice"] = dionice;
            
        }


        public IActionResult IndexSimple()
        {
            var autoceste = _context.Autocesta
                .AsNoTracking()
                .Include(a => a.IdUpraviteljaNavigation)
                .ToList();
            var dionice = _context.Dionica
                    .AsNoTracking()
                    
                    .ToList();

            ViewData["dionice"] = dionice;
            return View(autoceste);
        }

        public IActionResult IndexSimple2()
        {
            var autoceste = _context.Autocesta
                .AsNoTracking()
                .Select(m => new AutocestaViewModel
                {
                    Id = m.Id,
                    Naziv = m.Naziv,
                    DuljinaPlan = m.DuljinaPlan,
                    Duljina = m.Duljina,
                    Pocetak = m.Pocetak,
                    Kraj = m.Kraj,
                    IdUpravitelja = m.IdUpraviteljaNavigation.Id
                })
                .ToList();
            var model = new AutocesteViewModel
            {
                Autoceste = autoceste
            };
            var dionice = _context.Dionica
                    .AsNoTracking()
                    .ToList();

            ViewData["dionice"] = dionice;

           // logger.LogInformation("Učitavanje podataka o autocestama");
            return View(model);
        }


        // GET: Autocesta/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var autocesta = await _context.Autocesta
                .Include(a => a.IdUpraviteljaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (autocesta == null)
            {
                return NotFound();
            }

            return View(autocesta);
        }

        // GET: Autocesta/Create
        public IActionResult Create()
        {
            ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv");
            return View();
        }

        // POST: Autocesta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public async Task<IActionResult> Create([Bind("Id,Naziv,DuljinaPlan,Duljina,Pocetak,Kraj,IdUpravitelja")] Autocesta autocesta)
        {

            logger.LogTrace(JsonConvert.SerializeObject(autocesta));
            if (ModelState.IsValid && !AutocestaExists(autocesta.Id))
            {
                try
                {
                    _context.Add(autocesta);
                    await _context.SaveChangesAsync();
                    ViewData["message"] = $"Autocesta {autocesta.Naziv} je dodana u bazu";

                    logger.LogInformation(new EventId(1000), $"Autocesta {autocesta.Naziv} je dodana u bazu");
                    return RedirectToAction("IndexSimple2", ViewData["message"]);
                }catch (Exception exc)
                {
                   
                    logger.LogError("Pogreška prilikom dodavanje nove autoceste: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(autocesta);
                }

            }

            else
            {
                ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv", autocesta.IdUpravitelja);
                return View(autocesta);

            }



        }

        // GET: Autocesta/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var autocesta = await _context.Autocesta.FindAsync(id);
            if (autocesta == null)
            {
                return NotFound();
            }
            ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv", autocesta.IdUpravitelja);
            return View(autocesta);
        }


        // POST: Autocesta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,DuljinaPlan,Duljina,Pocetak,Kraj,IdUpravitelja")] Autocesta autocesta)
        {
            if (id != autocesta.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(autocesta);
                    await _context.SaveChangesAsync();

                    logger.LogInformation($"Autocesta {autocesta.Naziv} je ažurirana");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AutocestaExists(autocesta.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdUpravitelja"] = new SelectList(_context.Upravitelj, "Id", "Naziv", autocesta.IdUpravitelja);
            return View(autocesta);
        }


        [HttpGet]
        public IActionResult EditPartial(int id)
        {
            var autocesta = _context.Autocesta
                             .AsNoTracking()
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (autocesta != null)
            {
              
                return PartialView(autocesta);
            }
            else
            {
                return NotFound($"Neispravan id autoceste: {id}");
            }
        }


    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult EditPartial(Autocesta autocesta)
    {
        if (autocesta == null)
        {
            return NotFound("Nema poslanih podataka");
        }
        bool checkId = _context.Autocesta.Any(m => m.Id == autocesta.Id);
        if (!checkId)
        {
            return NotFound($"Neispravan id autoceste: {autocesta.Id}");
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(autocesta);
               _context.SaveChanges();
                logger.LogInformation(new EventId(1000), $"Autocesta {autocesta.Naziv} je ažurirana");
                return StatusCode(302, Url.Action(nameof(Row), new { id = autocesta.Id }));
                 
               }
            catch (Exception exc)
            {
                ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                return PartialView(autocesta);
            }
        }
        else
        {
            return PartialView(autocesta);
        }
    }


        public PartialViewResult Row(int id)
        {
            var autocesta = _context.Autocesta
                             .Where(m => m.Id == id)
                             .Select(m => new AutocestaViewModel
                             {
                                 Id = m.Id,
                                 Naziv = m.Naziv,
                                 DuljinaPlan = m.DuljinaPlan,
                                 Duljina = m.Duljina,
                                 Pocetak = m.Pocetak,
                                 Kraj = m.Kraj,
                                 IdUpravitelja = m.IdUpraviteljaNavigation.Id
               
                             })
                             .SingleOrDefault();
            if (autocesta != null)
            {
                return PartialView(autocesta);
            }
            else
            {
                //vratiti prazan sadržaj?
                return PartialView("ErrorMessageRow", $"Neispravan id autoceste: {id}");
            }
        }





        // GET: Autocesta/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var autocesta = await _context.Autocesta
                .Include(a => a.IdUpraviteljaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (autocesta == null)
            {
                return NotFound();
            }

            return View(autocesta);
        }
        
      // POST: Autocesta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var autocesta = await _context.Autocesta.FindAsync(id);
            if (autocesta != null)
            {
                try
                {
                    String naziv = autocesta.Naziv;
                    _context.Autocesta.Remove(autocesta);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Autocesta {naziv} uspješno obrisana");
                }
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom brisanja autoceste: " + exc.CompleteExceptionMessage());
                }
            }
            else
            {

                logger.LogWarning("Ne postoji autocesta s id-em: {0} ", id);
            }
            return RedirectToAction(nameof(Index));
        }
        
            
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePartial(int id)
        {
            var autocesta = _context.Autocesta
                             .AsNoTracking() //ima utjecaj samo za Update, za brisanje možemo staviti AsNoTracking
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (autocesta != null)
            {
                try
                {
                    string naziv = autocesta.Naziv;
                   _context.Remove(autocesta);
                    _context.SaveChanges();
                    logger.LogInformation($"Autocesta {naziv} uspješno obrisana");
                    var result = new
                    {
                        message = $"Autoceta {naziv} sa šifrom {id} obrisano.",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = "Pogreška prilikom brisanja autoceste:  " + exc.CompleteExceptionMessage(),
                        successful = false
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Mjesto sa šifrom {id} ne postoji");
            }
        }
        

  
        private bool AutocestaExists(int id)
        {
            return _context.Autocesta.Any(e => e.Id == id);
        }
    }
}
