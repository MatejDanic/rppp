﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.ViewModels;

namespace MojTimskiProjekt.Controllers
{
    public class ObjektController : Controller
    {
        private readonly RPPP14Context _context;
        private readonly AppSettings appData;
        private readonly ILogger logger;

        public ObjektController(RPPP14Context context, IOptionsSnapshot<AppSettings> options, ILogger<ObjektController> logger)
        {
            _context = context;
            appData = options.Value;
            this.logger = logger;
        }

        // GET: Objekt
        public async Task<IActionResult> Index()
        {
            var rPPP14Context = _context.Objekt
                                            .Include(a => a.IdDioniceNavigation)
                                            .Include(o => o.IdVrsteObjektaNavigation);
            var objekti = _context.Objekt
                                    .OrderBy(d => d.Naziv)
                                    .Select(d => new { d.Naziv, d.Id })
                                    .ToList();
            ViewData["Objekti"] = objekti;
            ViewBag.IdDionice = new SelectList(objekti, nameof(Objekt.IdDionice), nameof(Objekt.IdVrsteObjekta));
            return View(await rPPP14Context.ToListAsync());
        }


        public IActionResult IndexSimple()
        {
            var objekti = _context.Objekt
                .Include(o => o.IdDioniceNavigation)
                .Include(o => o.IdVrsteObjektaNavigation)
                .AsNoTracking()
                .Select(m => new ObjektViewModel
                {
                    Id = m.Id,
                    Naziv = m.Naziv,
                    Lokacija = m.Lokacija,
                    IdDionice = m.IdDionice,
                    IdVrsteObjekta = m.IdVrsteObjekta
                })
                .ToList();
            var model = new ObjektiViewModel
            {
                Objekti = objekti
            };
            ViewData["objekti"] = objekti;
            logger.LogInformation("Učitavanje podataka o objektima");
            return View(model);
        }



        public IActionResult IndexSimple2()
        {
            var objekti = _context.Objekt
                .AsNoTracking()
                .Include(a => a.IdDioniceNavigation)
                .Include(a => a.IdVrsteObjektaNavigation)
                .OrderBy(a => a.Id)
                .ToList();
            var uredaji = _context.Uredaj
                    .AsNoTracking()
                    .OrderBy(a => a.Naziv)
                    .ToList();

            ViewData["uredaji"] = uredaji;
            return View(objekti);
        }



        // GET: Objekt/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var objekt = await _context.Objekt
                .Include(o => o.IdDioniceNavigation)
                .Include(o => o.IdVrsteObjektaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (objekt == null)
            {
                return NotFound();
            }

            return View(objekt);
        }

        // GET: Objekt/Create
        public IActionResult Create()
        {
            var objekti = _context.Objekt
                                    .OrderBy(d => d.Naziv)
                                    .Select(d => new { d.Naziv, d.Id })
                                    .ToList();
            ViewData["Objekti"] = objekti;
            ViewBag.IdDionice = new SelectList(objekti, nameof(Objekt.IdDionice), nameof(Objekt.IdVrsteObjekta));
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Kraj");
            ViewData["IdVrsteObjekta"] = new SelectList(_context.VrstaObjekta, "Id", "Vrsta");
            ViewData["IdObjekta"] = new SelectList(_context.Objekt, "Id", "Naziv");
            return View();
        }

        // POST: Objekt/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,Lokacija,IdDionice,IdVrsteObjekta")] Objekt objekt)
        {
            logger.LogTrace(JsonConvert.SerializeObject(objekt));
            if (ModelState.IsValid && !ObjektExists(objekt.Id))
            {
                try
                {
                    _context.Add(objekt);
                    await _context.SaveChangesAsync();
                    ViewData["message"] = $"Objekt {objekt.Naziv} je dodan u bazu";
                    logger.LogInformation(new EventId(1000), $"Objekt {objekt.Naziv} je dodan u bazu");
                    return RedirectToAction(nameof(Index), ViewData["message"]);
                }
                catch(Exception exc)
                {

                    logger.LogError("Pogreška prilikom dodavanje novog objekta: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(objekt);
                }


            }
            else
            {
                ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Kraj", objekt.IdDionice);
                ViewData["IdVrsteObjekta"] = new SelectList(_context.VrstaObjekta, "Id", "Vrsta", objekt.IdVrsteObjekta);

                /*ViewData["IdObjekta"] = new SelectList(_context.Objekt, "Id", "Naziv", objekt.Id);
                ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Kraj", objekt.IdDionice);
                ViewData["IdVrsteObjekta"] = new SelectList(_context.VrstaObjekta, "Id", "Vrsta", objekt.IdVrsteObjekta);*/
                return View(objekt);
            }
        }

        // GET: Objekt/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var objekt = await _context.Objekt.FindAsync(id);
            if (objekt == null)
            {
                return NotFound();
            }
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Kraj", objekt.IdDionice);
            ViewData["IdVrsteObjekta"] = new SelectList(_context.VrstaObjekta, "Id", "Vrsta", objekt.IdVrsteObjekta);
            return View(objekt);
        }

        // POST: Objekt/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,Lokacija,IdDionice,IdVrsteObjekta")] Objekt objekt)
        {
            if (id != objekt.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(objekt);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ObjektExists(objekt.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Kraj", objekt.IdDionice);
            ViewData["IdVrsteObjekta"] = new SelectList(_context.VrstaObjekta, "Id", "Vrsta", objekt.IdVrsteObjekta);
            return View(objekt);
        }

        // GET: Objekt/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var objekt = await _context.Objekt
                .Include(o => o.IdDioniceNavigation)
                .Include(o => o.IdVrsteObjektaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (objekt == null)
            {
                return NotFound();
            }

            return View(objekt);
        }

        // POST: Objekt/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var objekt = await _context.Objekt.FindAsync(id);
            if (objekt != null)
            {
                try
                {
                    _context.Objekt.Remove(objekt);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Objekt {objekt.Naziv} uspješno obrisan");
                } 
                catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom brisanja objekta: " + exc.CompleteExceptionMessage());
                }
            }
            else
            {
                logger.LogWarning("Ne postoji objekt s id-em: {0} ", id);
            }
            return RedirectToAction(nameof(Index));
        }

        private bool ObjektExists(int id)
        {
            return _context.Objekt.Any(e => e.Id == id);
        }

        [HttpGet]
        public IActionResult EditPartial(int id)
        {
            var objekt = _context.Objekt
                             .AsNoTracking()
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (objekt != null)
            {

                return PartialView(objekt);
            }
            else
            {
                return NotFound($"Neispravan id objekta: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditPartial(Objekt objekt)
        {
            if (objekt == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = _context.Objekt.Any(m => m.Id == objekt.Id);
            if (!checkId)
            {
                return NotFound($"Neispravan id objekta: {objekt.Id}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(objekt);
                    _context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Objekt {objekt.Naziv} je ažuriran");
                    return StatusCode(302, Url.Action(nameof(Row), new { id = objekt.Id }));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return PartialView(objekt);
                }
            }
            else
            {
                return PartialView(objekt);
            }
        }

        public PartialViewResult Row(int id)
        {
            var objekt = _context.Objekt
                             .Where(m => m.Id == id)
                             .Select(m => new ObjektViewModel
                             {
                                 Id = m.Id,
                                 Naziv = m.Naziv,
                                 Lokacija = m.Lokacija,
                                 IdDionice = m.IdDionice,
                                 IdVrsteObjekta = m.IdVrsteObjekta
                             })
                             .SingleOrDefault();
            if (objekt != null)
            {
                return PartialView(objekt);
            }
            else
            {
                //vratiti prazan sadržaj?
                return PartialView("ErrorMessageRow", $"Neispravan id objekta: {id}");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePartial(int id)
        {
            var objekt = _context.Objekt
                             .AsNoTracking() //ima utjecaj samo za Update, za brisanje možemo staviti AsNoTracking
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (objekt != null)
            {
                try
                {
                    string naziv = objekt.Naziv;
                    _context.Remove(objekt);
                    _context.SaveChanges();
                    var result = new
                    {
                        message = $"objekt {naziv} sa šifrom {id} obrisan.",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = "Pogreška prilikom brisanja objekta:  " + exc.CompleteExceptionMessage(),
                        successful = false
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Mjesto sa šifrom {id} ne postoji");
            }
        }
    }
}
