﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MojTimskiProjekt.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using PdfRpt.ColumnsItemsTemplates;
using PdfRpt.Core.Contracts;
using PdfRpt.Core.Helper;
using PdfRpt.FluentInterface;
using MojTimskiProjekt.ViewModels;

namespace MojTimskiProjekt.Controllers
{
    public class ReportController : Controller
    {

        private readonly RPPP14Context ctx;
        private const string ExcelContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public ReportController(RPPP14Context ctx)
        {
            this.ctx = ctx;
        }


        #region Private methods
        private PdfReport CreateReport(string naslov)
        {
            var pdf = new PdfReport();

            pdf.DocumentPreferences(doc =>
            {
                doc.Orientation(PageOrientation.Portrait);
                doc.PageSize(PdfPageSize.A4);
                doc.DocumentMetadata(new DocumentMetadata
                {
                    Author = "RPPP14",
                    Application = "RPPP14 asp.NET Core",
                    Title = naslov
                });
                doc.Compression(new CompressionSettings
                {
                    EnableCompression = true,
                    EnableFullCompression = true
                });
            })
            .MainTableTemplate(template =>
            {
                template.BasicTemplate(BasicTemplate.ProfessionalTemplate);
            })
            .MainTablePreferences(table =>
            {
                table.ColumnsWidthsType(TableColumnWidthType.Relative);
          //table.NumberOfDataRowsPerPage(20);
          table.GroupsPreferences(new GroupsPreferences
                {
                    GroupType = GroupType.HideGroupingColumns,
                    RepeatHeaderRowPerGroup = true,
                    ShowOneGroupPerPage = true,
                    SpacingBeforeAllGroupsSummary = 5f,
                    NewGroupAvailableSpacingThreshold = 150,
                    SpacingAfterAllGroupsSummary = 5f
                });
                table.SpacingAfter(4f);
            });

            return pdf;
        }
        #endregion

        public async Task<IActionResult> AutocestePdf()
        {
            string naslov = "Popis autocesta s pripadajućim dionicama";

            
           
            /*left outer join: ne radi dobro 
            var auto = await (from autocesta in ctx.Autocesta
                        join dionica in ctx.Dionica
                        on autocesta.Id equals dionica.IdAutoceste into query
                        from item in query.DefaultIfEmpty()
                              select new AutocestaPdf
                        {
                            Id = autocesta.Id,
                            Naziv = autocesta.Naziv,
                            DuljinaPlan = autocesta.DuljinaPlan,
                            Duljina = autocesta.Duljina,
                            Pocetak = autocesta.Pocetak,
                            Kraj = autocesta.Kraj,
                            IdUpravitelja = autocesta.IdUpraviteljaNavigation.Id,
                            NazivUpravitelja = autocesta.IdUpraviteljaNavigation.Naziv,
                            Dionice = ctx.Dionica.Where(d => d.IdAutoceste == autocesta.Id).Select(d => d.Naziv).ToList(),
                            DioniceString = String.Join(", ", ctx.Dionica.Where(d => d.IdAutoceste == autocesta.Id).Select(d => d.Naziv).ToList())
                        }).OrderBy(a => a.Id).Include(a => a.IdUpraviteljaNavigation).ToListAsync(); */

            var auto2 = await ctx.Autocesta
                    .AsNoTracking()
                    .Include(a => a.IdUpraviteljaNavigation)
                    .Select(a => new AutocestaPdf {

                        Id = a.Id,
                        Naziv = a.Naziv,
                        DuljinaPlan = a.DuljinaPlan,
                        Duljina = a.Duljina,
                        Pocetak = a.Pocetak,
                        Kraj = a.Kraj,
                        IdUpravitelja = a.IdUpraviteljaNavigation.Id,
                        NazivUpravitelja = a.IdUpraviteljaNavigation.Naziv,
                        Dionice = ctx.Dionica.Where(d => d.IdAutoceste == a.Id).Select(d => d.Naziv).ToList(),
                        DioniceString = String.Join(", ", ctx.Dionica.Where(d => d.IdAutoceste == a.Id).Select(d => d.Naziv).ToList())
                    }).OrderBy(a => a.Id).Include(a => a.IdUpraviteljaNavigation).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(auto2));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {

                    column.PropertyName<AutocestaPdf>(x => x.Id);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id autoceste");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<AutocestaPdf>(x => x.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(2);
                    column.HeaderCell("Naziv autoceste", horizontalAlignment: HorizontalAlignment.Center);
                });

           

                columns.AddColumn(column =>
                {

                    column.PropertyName<AutocestaPdf>(a => a.DioniceString);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    
                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(2);
                    column.HeaderCell("Dionice autoceste", horizontalAlignment: HorizontalAlignment.Center);
  
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<AutocestaPdf>(x => x.Pocetak);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Početak autoceste", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<AutocestaPdf>(x => x.Kraj);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Kraj autoceste", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<AutocestaPdf>(x => x.Duljina);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Duljina autoceste (km)", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<AutocestaPdf>(x => x.NazivUpravitelja);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Upravitelj", horizontalAlignment: HorizontalAlignment.Center);
                });
            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Autoceste.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }



        public async Task<IActionResult> ObjektiPdf()
        {
            string naslov = "Popis objekata s pripadajućim uređajima";


            var objekti = await ctx.Objekt
                    .AsNoTracking()
                    .Include(a => a.IdDioniceNavigation)
                    .Include(a => a.IdVrsteObjektaNavigation)
                    .Select(a => new ObjektPdf
                    {

                        Id = a.Id,
                        Naziv = a.Naziv,
                        Lokacija = a.Lokacija,
                        IdDionice = a.IdDionice,
                        NazivDionice = a.IdDioniceNavigation.Naziv,
                        VrstaObjekta = a.IdVrsteObjektaNavigation.Vrsta,
                       
                        UredajiString = String.Join("\n", ctx.Uredaj.Where(d => d.IdObjekta == a.Id).Select(d => d.Naziv).ToList())
                    }).OrderBy(a => a.Id).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(objekti));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {

                    column.PropertyName<ObjektPdf>(x => x.Id);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id objekta");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ObjektPdf>(x => x.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(2);
                    column.Width(1);
                    column.HeaderCell("Naziv objekta", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<ObjektPdf>(a => a.VrstaObjekta);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Vrsta objekta", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ObjektPdf>(x => x.Lokacija);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Lokacija", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ObjektPdf>(x => x.NazivDionice);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(5);
                    column.Width(1);
                    column.HeaderCell("Naziv dionice", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ObjektPdf>(x => x.UredajiString);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(6);
                    column.Width(2);
                    column.HeaderCell("Uređaji", horizontalAlignment: HorizontalAlignment.Center);
                });

             
            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Objekti.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }




        public async Task<IActionResult> UredajPdf()
        {
            string naslov = "Popis uredaja s pripadajućim uređajima";


            var uredaji = await ctx.Uredaj
                    .AsNoTracking()
                    .Include(a => a.IdObjektaNavigation)
                    .Select(a => new UredajPdf
                    {

                        Id = a.Id,
                        Naziv = a.Naziv,
                        IdObjekta = a.IdObjekta,
                        IdVrsteUredaja = a.IdVrsteUredaja,
                        IdObjektaNavigation = a.IdObjektaNavigation,
                        IdVrsteUredajaNavigation = a.IdVrsteUredajaNavigation,
                        
                    }).OrderBy(a => a.Id).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(uredaji));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {

                    column.PropertyName<UredajPdf>(x => x.Id);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id uredaja");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UredajPdf>(x => x.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(2);
                    column.Width(1);
                    column.HeaderCell("Naziv uredaja", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<UredajPdf>(a => a.IdObjekta);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("ID objekta", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UredajPdf>(x => x.IdVrsteUredaja);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("ID vrste uredaja", horizontalAlignment: HorizontalAlignment.Center);
                });


            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Uredaji.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }




        public async Task<IActionResult> NaplatnaKucicaPdf()
        {
            string naslov = "Popis naplatnih kućica s izdanim računima";

            var kucice= await ctx.NaplatnaKucica
                    .AsNoTracking()
                    .Include(a => a.IdNaplatnePostajeNavigation)
                    .Select(a => new NaplatnaKucicaPdf
                    {

                        Id = a.Id,
                        NazivKucice = a.Naziv,
                        NazivPostaje = a.IdNaplatnePostajeNavigation.Vrsta,
                      
                        RacuniString = String.Join(" \n", ctx.Racun.Include(d => d.IdKategorijeVozilaNavigation).Where(d => d.IdNaplatneKucice == a.Id)
                            .Select(d => new {d.MjestoUlaska, d.DatumUlaz, d.MjestoIzlaska, d.DatumIzlaz, d.IdKategorijeVozilaNavigation.KategorijaVozila1 , d.RegistracijskeOznake, d.Iznos }).ToList())
                    }).OrderBy(a => a.Id).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(kucice));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {

                    column.PropertyName<NaplatnaKucicaPdf>(x => x.Id);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id naplatne kucice");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<NaplatnaKucicaPdf>(x => x.NazivKucice);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Naziv naplatne kucice", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<NaplatnaKucicaPdf>(a => a.NazivPostaje);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Naziv naplatne postaje", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<NaplatnaKucicaPdf>(x => x.RacuniString);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(3);
                    column.HeaderCell("Izdani racuni", horizontalAlignment: HorizontalAlignment.Center);
                });

               
            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=NaplatneKucice.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }





        public async Task<IActionResult> BlagajnikPdf()
        {
            string naslov = "Popis blagajnika s izdanim računima";

            //left outer join 
            /*var auto = await (from blagajnik in ctx.Blagajnik
                              join racun in ctx.Racun
                              on blagajnik.Id equals racun.IdBlagajnika into query
                              from item in query.DefaultIfEmpty(new Racun {})
                              select new BlagajnikPdf
                              {
                                  Id = blagajnik.Id,
                                  IdNaplatneKućice = blagajnik.IdNaplatneKućice,
                                  Ime = blagajnik.IdZaposlenikaNavigation.Ime,
                                  Prezime = blagajnik.IdZaposlenikaNavigation.Prezime,
                                  OIB = blagajnik.IdZaposlenikaNavigation.Oib,
                                  Racuni = ctx.Racun.Where(d => d.IdBlagajnika == blagajnik.Id).Select(d => d.RegistracijskeOznake).ToList(),
                                  RacuniString = String.Join(", ", ctx.Racun.Where(d => d.IdBlagajnika == blagajnik.Id).Select(d => d.RegistracijskeOznake).ToList())
                              }).OrderBy(a => a.Id).Include(a => a.IdZaposlenikaNavigation).ToListAsync();*/

            var auto2 = await ctx.Blagajnik
                    .AsNoTracking()
                    .Include(a => a.IdZaposlenikaNavigation)
                    .Select(a => new BlagajnikPdf
                    {

                        Id = a.Id,
                        IdNaplatneKućice = a.IdNaplatneKućice,
                        Ime = a.IdZaposlenikaNavigation.Ime,
                        Prezime = a.IdZaposlenikaNavigation.Prezime,
                        OIB = a.IdZaposlenikaNavigation.Oib,
                        NaplatnaKucica = a.IdNaplatneKućiceNavigation.Naziv,
                        //Racuni = ctx.Racun.Where(d => d.IdBlagajnika == a.Id).Select(d => d.RegistracijskeOznake).ToList(),
                        RacuniString = String.Join(", ", ctx.Racun.Where(d => d.IdBlagajnika == a.Id).Select(d => new { d.DatumIzlaz, d.RegistracijskeOznake, d.Iznos }).ToList())// + String.Join(", ", ctx.Racun.Where(d => d.IdBlagajnika == a.Id).Select(d => d.Iznos).ToList())
                    }).OrderBy(a => a.Id).Include(a => a.IdZaposlenikaNavigation).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(auto2));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {
                    column.PropertyName(nameof(Blagajnik.Id));
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id blagajnika");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<BlagajnikPdf>(x => x.Ime);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(2);
                    column.HeaderCell("Ime", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<BlagajnikPdf>(a => a.Prezime);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Prezime", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<BlagajnikPdf>(x => x.OIB);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("OIB", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<BlagajnikPdf>(x => x.NaplatnaKucica);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Naplatna kućica", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<BlagajnikPdf>(x => x.RacuniString);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Racuni", horizontalAlignment: HorizontalAlignment.Center);
                });
                
            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Blagajnici.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }

        public async Task<IActionResult> OperaterPdf()
        {
            string naslov = "Popis operatera";

            //left outer join 
            /*var auto = await (from blagajnik in ctx.Blagajnik
                              join racun in ctx.Racun
                              on blagajnik.Id equals racun.IdBlagajnika into query
                              from item in query.DefaultIfEmpty(new Racun {})
                              select new BlagajnikPdf
                              {
                                  Id = blagajnik.Id,
                                  IdNaplatneKućice = blagajnik.IdNaplatneKućice,
                                  Ime = blagajnik.IdZaposlenikaNavigation.Ime,
                                  Prezime = blagajnik.IdZaposlenikaNavigation.Prezime,
                                  OIB = blagajnik.IdZaposlenikaNavigation.Oib,
                                  Racuni = ctx.Racun.Where(d => d.IdBlagajnika == blagajnik.Id).Select(d => d.RegistracijskeOznake).ToList(),
                                  RacuniString = String.Join(", ", ctx.Racun.Where(d => d.IdBlagajnika == blagajnik.Id).Select(d => d.RegistracijskeOznake).ToList())
                              }).OrderBy(a => a.Id).Include(a => a.IdZaposlenikaNavigation).ToListAsync();*/

            var auto2 = await ctx.Operater
                    .AsNoTracking()
                    .Include(a => a.IdZaposlenikaNavigation)
                    .Select(a => new OperaterPdf
                    {

                        Id = a.Id,
                        IdDionice = a.IdDionice,
                        Ime = a.IdZaposlenikaNavigation.Ime,
                        Prezime = a.IdZaposlenikaNavigation.Prezime,
                        OIB = a.IdZaposlenikaNavigation.Oib,
                        Dionica = a.IdDioniceNavigation.Pocetak + " - " + a.IdDioniceNavigation.Kraj,
                        //Racuni = ctx.Racun.Where(d => d.IdBlagajnika == a.Id).Select(d => d.RegistracijskeOznake).ToList(),
                        //RacuniString = String.Join(", ", ctx.Racun.Where(d => d.IdBlagajnika == a.Id).Select(d => new { d.DatumIzlaz, d.RegistracijskeOznake, d.Iznos }).ToList())// + String.Join(", ", ctx.Racun.Where(d => d.IdBlagajnika == a.Id).Select(d => d.Iznos).ToList())
                    }).OrderBy(a => a.Id).Include(a => a.IdZaposlenikaNavigation).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(auto2));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {
                    column.PropertyName(nameof(Operater.Id));
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id Operatera");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<OperaterPdf>(x => x.Ime);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(2);
                    column.HeaderCell("Ime", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<OperaterPdf>(a => a.Prezime);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Prezime", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<OperaterPdf>(x => x.OIB);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("OIB", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<OperaterPdf>(x => x.Dionica);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Dionica", horizontalAlignment: HorizontalAlignment.Center);
                });

            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Operateri.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }

        public async Task<IActionResult> ZaposlenikPdf()
        {
            string naslov = "Popis Zaposlenika";

            //left outer join 
            

            var auto2 = await ctx.Zaposlenik
                    .AsNoTracking()
                    .Include(a => a.IdUpraviteljaNavigation)
                    .Select(a => new ZaposlenikPdf
                    {

                        Id = a.Id,
                        Ime = a.Ime,
                        Prezime = a.Prezime,
                        Oib = a.Oib,
                        Upravitelj = a.IdUpraviteljaNavigation.Naziv,
                        VrstaPosla = a.IdVrstePoslaNavigation.Vrsta
                        //Racuni = ctx.Racun.Where(d => d.IdBlagajnika == a.Id).Select(d => d.RegistracijskeOznake).ToList(),
                        //RacuniString = String.Join(", ", ctx.Racun.Where(d => d.IdBlagajnika == a.Id).Select(d => d.RegistracijskeOznake).ToList())// + String.Join(", ", ctx.Racun.Where(d => d.IdBlagajnika == a.Id).Select(d => d.Iznos).ToList())
                    }).OrderBy(a => a.Id).Include(a => a.IdUpraviteljaNavigation).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(auto2));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {
                    column.PropertyName(nameof(Zaposlenik.Id));
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id zaposlenika");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ZaposlenikPdf>(x => x.Ime);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(2);
                    column.HeaderCell("Ime", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<ZaposlenikPdf>(a => a.Prezime);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Prezime", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ZaposlenikPdf>(x => x.Oib);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("OIB", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ZaposlenikPdf>(x => x.Upravitelj);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Upravitelj", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ZaposlenikPdf>(x => x.VrstaPosla);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Vrsta posla", horizontalAlignment: HorizontalAlignment.Center);
                });

            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Zaposlenici.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }

        public async Task<IActionResult> UpraviteljPdf()
        {
            string naslov = "Popis Upravitelja";

            //left outer join 


            var auto2 = await ctx.Upravitelj
                    .AsNoTracking()
                    .Select(a => new UpraviteljPdf
                    {
                        Id = a.Id,
                        Naziv = a.Naziv,
                        Sjediste = a.Sjediste,
                        Oib = a.Oib,
                        EMail = a.EMail,
                        Kontakt = a.Kontakt
                    }).OrderBy(a => a.Id).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(auto2));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {
                    column.PropertyName(nameof(Upravitelj.Id));
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id upravitelja");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UpraviteljPdf>(x => x.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(2);
                    column.HeaderCell("Naziv", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<UpraviteljPdf>(a => a.Sjediste);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Sjediste", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UpraviteljPdf>(x => x.Oib);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("OIB", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UpraviteljPdf>(x => x.EMail);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("EMail", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UpraviteljPdf>(x => x.Kontakt);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Kontakt", horizontalAlignment: HorizontalAlignment.Center);
                });

            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Upravitelji.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }


        public async Task<IActionResult> ObjektPdf()
        {
            string naslov = "Popis objekata";

            //left outer join 


            var auto2 = await ctx.Objekt
                    .AsNoTracking()
                    .Select(a => new ObjektPdf
                    {
                        Id = a.Id,
                        Naziv = a.Naziv,
                        Lokacija = a.Lokacija,
                        IdDionice = a.IdDionice,
                        IdVrsteObjekta = a.IdVrsteObjekta,
                    }).OrderBy(a => a.Id).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(auto2));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {
                    column.PropertyName(nameof(Objekt.Id));
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id objekta");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ObjektPdf>(x => x.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(2);
                    column.HeaderCell("Naziv", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<ObjektPdf>(a => a.Lokacija);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Lokacija", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ObjektPdf>(x => x.IdDionice);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Id dionice", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ObjektPdf>(x => x.IdVrsteObjekta);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Id vrste objekta", horizontalAlignment: HorizontalAlignment.Center);
                });

            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Objekti.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }

        public async Task<IActionResult> ProslaStanjaPdf()
        {
            string naslov = "Popis stanja na autocestama";

            //left outer join 


            var auto2 = await ctx.ProslaStanja
                    .AsNoTracking()
                    .Select(a => new ProslaStanjaPdf
                    {
                        Id = a.Id,
                        DatumOd = a.DatumOd,
                        DatumDo = a.DatumDo,
                        Opis = a.Opis,
                        IdAutoceste = a.IdAutoceste,
                        IdDionice = a.IdDionice,
                        NazivAutoceste = a.IdAutocesteNavigation.Naziv,
                        NazivDionice = a.IdDioniceNavigation.Naziv
                       
                    }).OrderBy(a => a.Id).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(auto2));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {
                    column.PropertyName(nameof(ProslaStanja.Id));
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id stanja");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ProslaStanjaPdf>(x => x.DatumOd);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(2);
                    column.HeaderCell("Datum od", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<ProslaStanjaPdf>(a => a.DatumDo);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Datum do", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ProslaStanjaPdf>(x => x.Opis);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Opis", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ProslaStanjaPdf>(x => x.NazivAutoceste);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Naziv autoceste", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<ProslaStanjaPdf>(x => x.NazivDionice);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Naziv dionice", horizontalAlignment: HorizontalAlignment.Center);
                });

            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Stanja.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }


        #region Export u Excel
        public async Task<IActionResult> DioniceExcel()
        {
            var dionice = await ctx.Dionica
                                  .AsNoTracking()
                                  .Include(d => d.IdAutocesteNavigation)
                                  .OrderBy(d => d.Id)
                                  .ToListAsync();
            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis dionica";
                excel.Workbook.Properties.Author = "RPPP14-Barbara";
                var worksheet = excel.Workbook.Worksheets.Add("Dionice");

                //First add the headers
                worksheet.Cells[1, 1].Value = "Id dionice";
                worksheet.Cells[1, 2].Value = "Naziv dionice";
                worksheet.Cells[1, 3].Value = "Duljina dionice";
                worksheet.Cells[1, 4].Value = "Početak dionice";
                worksheet.Cells[1, 5].Value = "Kraj dionice";
                worksheet.Cells[1, 6].Value = "Id autoceste";
                worksheet.Cells[1, 7].Value = "Naziv autoceste";

                for (int i = 0; i < dionice.Count; i++)
                {
                    worksheet.Cells[i + 2, 1].Value = dionice[i].Id;
                    worksheet.Cells[i + 2, 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[i + 2, 2].Value = dionice[i].Naziv;
                    worksheet.Cells[i + 2, 3].Value = dionice[i].Duljina;
                    worksheet.Cells[i + 2, 4].Value = dionice[i].Pocetak;
                    worksheet.Cells[i + 2, 5].Value = dionice[i].Kraj;
                    worksheet.Cells[i + 2, 6].Value = dionice[i].IdAutocesteNavigation.Id;
                    worksheet.Cells[i + 2, 7].Value = dionice[i].IdAutocesteNavigation.Naziv;
                }

                worksheet.Cells[1, 1, dionice.Count + 1, 7].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "dionice.xlsx");
        }
        #endregion


        //Izvoz složenih podataka (1:N) u Excel
        #region 
        public async Task<IActionResult> AutocesteExcel()
        {
            var dionice = await ctx.Dionica
                .AsNoTracking()
                    .Select(a => new { a.Naziv, a.IdAutoceste})
                    .OrderBy(a => a.IdAutoceste)
                    .ToListAsync();
            var autoceste = await ctx.Autocesta
                .AsNoTracking()
                .Include(a => a.IdUpraviteljaNavigation)
                .OrderBy(a => a.Id)
                .ToListAsync();
            
            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis autocesta";
                excel.Workbook.Properties.Author = "RPPP14-Barbara";
                var worksheet = excel.Workbook.Worksheets.Add("Autoceste");

                //First add the headers
                worksheet.Cells[1, 1].Value = "Naziv";
                worksheet.Cells[1, 2].Value = "Dionice";
                worksheet.Cells[1, 3].Value = "Duljina ";
                worksheet.Cells[1, 4].Value = "Početak ";
                worksheet.Cells[1, 5].Value = "Kraj ";
                worksheet.Cells[1, 6].Value = "Naziv upravitelja";

                int cnt= 0;
                foreach(var autocesta in autoceste)
                {
                    worksheet.Cells[cnt + 2 , 1].Value = autocesta.Naziv;
                    worksheet.Cells[cnt + 2, 3].Value = autocesta.Duljina;
                    worksheet.Cells[cnt + 2, 4].Value = autocesta.Pocetak;
                    worksheet.Cells[cnt + 2, 5].Value = autocesta.Kraj;
                    worksheet.Cells[cnt + 2, 6].Value = autocesta.IdUpraviteljaNavigation.Naziv;
                    foreach (var dionica in dionice)
                    {
                        if(dionica.IdAutoceste == autocesta.Id)
                        {
                            worksheet.Cells[cnt + 2, 2].Value = dionica.Naziv;
                            cnt++;
                        }
                    }


                }

            //worksheet.Cells[1, 1, autoceste.Count + 10, 20].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "autoceste.xlsx");
        }
        #endregion


        #region 
        public async Task<IActionResult> IncidentExcel()
        {
           
            var incidenti = await ctx.Incident
                .AsNoTracking()
                .Include(a => a.IdUredajaNavigation)
                .OrderBy(a => a.Id)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis incidenata";
                excel.Workbook.Properties.Author = "RPPP14-Filip Kovačević";
                var worksheet = excel.Workbook.Worksheets.Add("Incidenti");

                //First add the headers
                worksheet.Cells[1, 1].Value = "Id Incidenta";
                worksheet.Cells[1, 2].Value = "Naziv Incidenta";
                worksheet.Cells[1, 3].Value = "Lokacija";
                worksheet.Cells[1, 4].Value = "Opis ";
                worksheet.Cells[1, 5].Value = "Id uređaja";
                worksheet.Cells[1, 6].Value = "Naziv uređaja";

                for (int i = 0; i < incidenti.Count; i++)
                {
                    worksheet.Cells[i + 2, 1].Value = incidenti[i].Id;
                    worksheet.Cells[i + 2, 2].Value = incidenti[i].Naziv;
                    worksheet.Cells[i + 2, 3].Value = incidenti[i].Lokacija;
                    worksheet.Cells[i + 2, 4].Value = incidenti[i].Opis;
                    worksheet.Cells[i + 2, 5].Value = incidenti[i].IdUredajaNavigation.Id;
                    worksheet.Cells[i + 2, 6].Value = incidenti[i].IdUredajaNavigation.Naziv;


                }
                //worksheet.Cells[1, 1, autoceste.Count + 10, 20].AutoFitColumns();
                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "incidenti.xlsx");
        }
        #endregion





        //Izvoz složenih podataka (1:N) u Excel
        #region 
        public async Task<IActionResult> NaplatnaKucicaExcel()
        {
            var racuni = await ctx.Racun
                .AsNoTracking()
                .Include(a => a.IdNaplatneKuciceNavigation)
                    .Select(d => new { d.MjestoUlaska, d.DatumUlaz, d.MjestoIzlaska, d.DatumIzlaz, d.IdKategorijeVozilaNavigation.KategorijaVozila1, d.RegistracijskeOznake, d.Iznos, d.IdNaplatneKuciceNavigation.Id })
                    .ToListAsync();
            var kucice = await ctx.NaplatnaKucica
                .Include(a => a.IdNaplatnePostajeNavigation)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis naplatnih kućica";
                excel.Workbook.Properties.Author = "RPPP14-Filip Fišić";
                var worksheet = excel.Workbook.Worksheets.Add("Naplatne kućice");

                //First add the headers
                worksheet.Cells[1, 1].Value = "Naziv naplatne kućice";
                worksheet.Cells[1, 2].Value = "Naziv naplatne postaje";
                worksheet.Cells[1, 3].Value = "Izdani računi";


                for (int i = 0; i < kucice.Count; i++)
                {
                    worksheet.Cells[i + 2, 1].Value = kucice[i].Naziv;
                    worksheet.Cells[i + 2, 2].Value = kucice[i].IdNaplatnePostajeNavigation.Lokacija;
            
                    String sviRacuni = "";
                    foreach (var racun in racuni)
                    {
                        if (racun.Id == kucice[i].Id)
                        {
                            sviRacuni += racun;
                            sviRacuni += "\n";
                        }
                    }
                    worksheet.Cells[i + 2, 3].Value = sviRacuni;
                    sviRacuni = "";
                   
                }

                worksheet.Cells[1, 1, kucice.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "naplatnekucice.xlsx");
        }
        #endregion







        //Izvoz složenih podataka (1:N) u Excel
        #region 
        public async Task<IActionResult> BlagajnikExcel()
        {
            var racuni = await ctx.Racun
                .AsNoTracking()
                    .Select(a => new { a.IdBlagajnika, a.MjestoUlaska, a.DatumUlaz, a.MjestoIzlaska, a.DatumIzlaz, a.Iznos, a.RegistracijskeOznake })
                    .OrderBy(a => a.IdBlagajnika)
                    .ToListAsync();
            var blagajnici = await ctx.Blagajnik
                .AsNoTracking()
                .Include(a => a.IdZaposlenikaNavigation)
                .Include(a => a.IdNaplatneKućiceNavigation)
                .OrderBy(a => a.Id)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis Blagajnika";
                excel.Workbook.Properties.Author = "RPPP14-Matej";
                var worksheet = excel.Workbook.Worksheets.Add("Blagajnici");

                //First add the headers
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "Ime";
                worksheet.Cells[1, 3].Value = "Prezime ";
                worksheet.Cells[1, 4].Value = "OIB ";
                worksheet.Cells[1, 6].Value = "Racuni ";
                worksheet.Cells[1, 5].Value = "Naplatna Kućica";

                int cnt = 0;

                foreach (var blagajnik in blagajnici)
                {
                    Boolean imaRacun = false;
                    worksheet.Cells[cnt + 2, 1].Value = blagajnik.Id;
                    worksheet.Cells[cnt + 2, 2].Value = blagajnik.IdZaposlenikaNavigation.Ime;
                    worksheet.Cells[cnt + 2, 3].Value = blagajnik.IdZaposlenikaNavigation.Prezime;
                    worksheet.Cells[cnt + 2, 4].Value = blagajnik.IdZaposlenikaNavigation.Oib;
                    worksheet.Cells[cnt + 2, 5].Value = blagajnik.IdNaplatneKućiceNavigation.Naziv;
                    foreach (var racun in racuni)
                    {
                        if (racun.IdBlagajnika == blagajnik.Id)
                        {
                            imaRacun = true;
                            worksheet.Cells[cnt + 2, 6].Value = racun.MjestoUlaska + " - " + racun.DatumUlaz + " - " + racun.MjestoIzlaska + " - " + racun.DatumIzlaz + " - " + racun.RegistracijskeOznake + " - " + racun.Iznos;
                            cnt++;
                        }
                    }
                    if(!imaRacun)
                    {
                        cnt++;
                    }

                }

                //  worksheet.Cells[1, 1, autoceste.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "blagajnici.xlsx");
        }
        #endregion

        //Izvoz složenih podataka (1:N) u Excel
        #region 
        public async Task<IActionResult> OperaterExcel()
        {
            var operateri = await ctx.Operater
                .AsNoTracking()
                .Include(a => a.IdZaposlenikaNavigation)
                .Include(a => a.IdDioniceNavigation)
                .OrderBy(a => a.Id)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis Operatera";
                excel.Workbook.Properties.Author = "RPPP14-Matej";
                var worksheet = excel.Workbook.Worksheets.Add("Operateri");

                //First add the headers
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "Ime";
                worksheet.Cells[1, 3].Value = "Prezime ";
                worksheet.Cells[1, 4].Value = "OIB ";
                worksheet.Cells[1, 5].Value = "Dionica";

                int cnt = 0;
                foreach (var operater in operateri)
                {
                    worksheet.Cells[cnt + 2, 1].Value = operater.Id;
                    worksheet.Cells[cnt + 2, 2].Value = operater.IdZaposlenikaNavigation.Ime;
                    worksheet.Cells[cnt + 2, 3].Value = operater.IdZaposlenikaNavigation.Prezime;
                    worksheet.Cells[cnt + 2, 4].Value = operater.IdZaposlenikaNavigation.Oib;
                    worksheet.Cells[cnt + 2, 5].Value = operater.IdDioniceNavigation.Pocetak + " - " + operater.IdDioniceNavigation.Kraj;
                    cnt++;

                }

                //  worksheet.Cells[1, 1, autoceste.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "operateri.xlsx");
        }
        #endregion

        //Izvoz složenih podataka (1:N) u Excel
        #region 
        public async Task<IActionResult> ZaposlenikExcel()
        {
            
            var zaposlenici = await ctx.Zaposlenik
                .AsNoTracking()
                .Include(a => a.IdUpraviteljaNavigation)
                .Include(a => a.IdVrstePoslaNavigation)
                .OrderBy(a => a.Id)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis Zaposlenika";
                excel.Workbook.Properties.Author = "RPPP14-Matej";
                var worksheet = excel.Workbook.Worksheets.Add("Zaposlenici");

                //First add the headers
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "Ime";
                worksheet.Cells[1, 3].Value = "Prezime ";
                worksheet.Cells[1, 4].Value = "Adresa";
                worksheet.Cells[1, 5].Value = "OIB ";
                worksheet.Cells[1, 6].Value = "Upravitelj ";
                worksheet.Cells[1, 7].Value = "Vrsta Posla";

                int cnt = 0;
                foreach (var zaposlenik in zaposlenici)
                {
                    worksheet.Cells[cnt + 2, 1].Value = zaposlenik.Id;
                    worksheet.Cells[cnt + 2, 2].Value = zaposlenik.Ime;
                    worksheet.Cells[cnt + 2, 3].Value = zaposlenik.Prezime;
                    worksheet.Cells[cnt + 2, 4].Value = zaposlenik.Adresa;
                    worksheet.Cells[cnt + 2, 5].Value = zaposlenik.Oib;
                    worksheet.Cells[cnt + 2, 6].Value = zaposlenik.IdUpraviteljaNavigation.Naziv;
                    worksheet.Cells[cnt + 2, 7].Value = zaposlenik.IdVrstePoslaNavigation.Vrsta;
                    cnt++;
                }

                //  worksheet.Cells[1, 1, autoceste.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "zaposlenici.xlsx");
        }
        #endregion

        #region 
        public async Task<IActionResult> UpraviteljExcel()
        {

            var upravitelji = await ctx.Upravitelj
                .AsNoTracking()
                .OrderBy(a => a.Id)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis Upravitelja";
                excel.Workbook.Properties.Author = "RPPP14-Kristijan";
                var worksheet = excel.Workbook.Worksheets.Add("Upravitelj");

                //First add the headers
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "Naziv";
                worksheet.Cells[1, 3].Value = "Sjediste ";
                worksheet.Cells[1, 4].Value = "OIB";
                worksheet.Cells[1, 5].Value = "EMail ";
                worksheet.Cells[1, 6].Value = "Kontakt ";

                int cnt = 0;
                foreach (var upravitelj in upravitelji)
                {
                    worksheet.Cells[cnt + 2, 1].Value = upravitelj.Id;
                    worksheet.Cells[cnt + 2, 2].Value = upravitelj.Naziv;
                    worksheet.Cells[cnt + 2, 3].Value = upravitelj.Sjediste;
                    worksheet.Cells[cnt + 2, 4].Value = upravitelj.Oib.ToString();
                    worksheet.Cells[cnt + 2, 5].Value = upravitelj.EMail;
                    worksheet.Cells[cnt + 2, 6].Value = upravitelj.Kontakt.ToString();
                    cnt++;
                }

                //  worksheet.Cells[1, 1, autoceste.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "upravitelji.xlsx");
        }
        #endregion

        #region 
        public async Task<IActionResult> ObjektExcel()
        {

            var objekti = await ctx.Objekt
                .AsNoTracking()
                .OrderBy(a => a.Id)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis objekata";
                excel.Workbook.Properties.Author = "RPPP14-Matej";
                var worksheet = excel.Workbook.Worksheets.Add("Objekt");

                //First add the headers
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "Naziv";
                worksheet.Cells[1, 3].Value = "Lokacija ";
                worksheet.Cells[1, 4].Value = "IdDionice";
                worksheet.Cells[1, 5].Value = "IdVrsteObjekta ";

                int cnt = 0;
                foreach (var objekt in objekti)
                {
                    worksheet.Cells[cnt + 2, 1].Value = objekt.Id;
                    worksheet.Cells[cnt + 2, 2].Value = objekt.Naziv;
                    worksheet.Cells[cnt + 2, 3].Value = objekt.Lokacija;
                    worksheet.Cells[cnt + 2, 4].Value = objekt.IdDionice.ToString();
                    worksheet.Cells[cnt + 2, 5].Value = objekt.IdVrsteObjekta;
                    cnt++;
                }

                //  worksheet.Cells[1, 1, autoceste.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "objekti.xlsx");
        }
        #endregion

        //Izvoz složenih podataka (1:N) u Excel
        #region 
        public async Task<IActionResult> UredajExcel()
        {

            var uredaji = await ctx.Uredaj
                .AsNoTracking()
                .OrderBy(a => a.Id)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis uredaja";
                excel.Workbook.Properties.Author = "RPPP14-Matej";
                var worksheet = excel.Workbook.Worksheets.Add("Uredaj");

                //First add the headers
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "Naziv";
                worksheet.Cells[1, 3].Value = "IdObjekta";
                worksheet.Cells[1, 4].Value = "IdVrsteUredaja";

                int cnt = 0;
                foreach (var uredaj in uredaji)
                {
                    worksheet.Cells[cnt + 2, 1].Value = uredaj.Id;
                    worksheet.Cells[cnt + 2, 2].Value = uredaj.Naziv;
                    worksheet.Cells[cnt + 2, 3].Value = uredaj.IdObjekta;
                    worksheet.Cells[cnt + 2, 4].Value = uredaj.IdVrsteUredaja.ToString();
                    cnt++;
                }

                //  worksheet.Cells[1, 1, autoceste.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "uredaji.xlsx");
        }
        #endregion


        //Izvoz složenih podataka (1:N) u Excel
        #region 
        public async Task<IActionResult> ProslaStanjaExcel()
        {

            var proslaStanja = await ctx.ProslaStanja
                .AsNoTracking()
                .Include(a => a.IdAutocesteNavigation)
                .Include(a => a.IdDioniceNavigation)
                .OrderBy(a => a.Id)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis stanja";
                excel.Workbook.Properties.Author = "RPPP14-Kristijan";
                var worksheet = excel.Workbook.Worksheets.Add("ProslaStanja");

                //First add the headers
                worksheet.Cells[1, 1].Value = "Id stanja";
                worksheet.Cells[1, 2].Value = "Datum od";
                worksheet.Cells[1, 3].Value = "Datum do ";
                worksheet.Cells[1, 4].Value = "Opis";
                worksheet.Cells[1, 5].Value = "Naziv autoceste ";
                worksheet.Cells[1, 6].Value = "Naziv dionice";

                int cnt = 0;
                foreach (var prosloStanje in proslaStanja)
                {
                    worksheet.Cells[cnt + 2, 1].Value = prosloStanje.Id;
                    worksheet.Cells[cnt + 2, 2].Value = prosloStanje.DatumOd.ToString();
                    worksheet.Cells[cnt + 2, 3].Value = prosloStanje.DatumDo.ToString();
                    worksheet.Cells[cnt + 2, 4].Value = prosloStanje.Opis;
                    worksheet.Cells[cnt + 2, 5].Value = prosloStanje.IdAutocesteNavigation.Naziv;
                    worksheet.Cells[cnt + 2, 6].Value = prosloStanje.IdDioniceNavigation.Naziv;
                    cnt++;
                }

                //  worksheet.Cells[1, 1, autoceste.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "proslaStanja.xlsx");
        }
        #endregion


        //Izvoz složenih podataka (1:N) u Excel
        #region 
        public async Task<IActionResult> AutocesteExcel2()
        {
            var dionice = await ctx.Dionica
                .AsNoTracking()
                    .Select(a => new { a.Naziv, a.IdAutoceste })
                    .OrderBy(a => a.IdAutoceste)
                    .ToListAsync();
            var autoceste = await ctx.Autocesta
                .AsNoTracking()
                .Include(a => a.IdUpraviteljaNavigation)
                .OrderBy(a => a.Id)
                .ToListAsync();

          /*  var auto = await (from autocesta in ctx.Autocesta
                              join dionica in ctx.Dionica
                              on autocesta.Id equals dionica.IdAutoceste into query
                              from item in query.DefaultIfEmpty(new Dionica { Naziv = String.Empty })
                              select new AutocestaPdf
                              {
                                  Id = autocesta.Id,
                                  Naziv = autocesta.Naziv,
                                  DuljinaPlan = autocesta.DuljinaPlan,
                                  Duljina = autocesta.Duljina,
                                  Pocetak = autocesta.Pocetak,
                                  Kraj = autocesta.Kraj,
                                  IdUpravitelja = autocesta.IdUpraviteljaNavigation.Id,
                                  NazivUpravitelja = autocesta.IdUpraviteljaNavigation.Naziv,
                                  Dionice = ctx.Dionica.Where(d => d.IdAutoceste == autocesta.Id).Select(d => d.Naziv).ToList(),
                                  DioniceString = String.Join(" ", ctx.Dionica.Where(d => d.IdAutoceste == autocesta.Id).Select(d => d.Naziv).ToList())
                              }).OrderBy(a => a.Id).Include(a => a.IdUpraviteljaNavigation).ToListAsync(); */

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis autocesta";
                excel.Workbook.Properties.Author = "RPPP14-Barbara";
                var worksheet = excel.Workbook.Worksheets.Add("Autoceste");

                worksheet.Cells[1, 1].Value = "Naziv";
                worksheet.Cells[1, 2].Value = "Dionice";
                worksheet.Cells[1, 3].Value = "Duljina ";
                worksheet.Cells[1, 4].Value = "Početak ";
                worksheet.Cells[1, 5].Value = "Kraj ";
                worksheet.Cells[1, 6].Value = "Naziv upravitelja";

                for (int i = 0; i < autoceste.Count; i++)
                {
                    worksheet.Cells[i + 2, 1].Value = autoceste[i].Naziv;
                    String sveDionice = "";
                    foreach(var dionica in dionice)
                    {
                        if(dionica.IdAutoceste == autoceste[i].Id)
                        {
                            sveDionice += dionica.Naziv;
                            sveDionice += "\n";
                        }
                    }
                    worksheet.Cells[i + 2, 2].Value = sveDionice;
                    sveDionice = "";
                    worksheet.Cells[i + 2, 3].Value = autoceste[i].Duljina;
                    worksheet.Cells[i + 2, 4].Value = autoceste[i].Pocetak;
                    worksheet.Cells[i + 2, 5].Value = autoceste[i].Kraj;
                    worksheet.Cells[i + 2, 6].Value = autoceste[i].IdUpraviteljaNavigation.Naziv;
                }

            
                worksheet.Cells[1, 1, autoceste.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
         return File(content, ExcelContentType, "autoceste.xlsx");
    
        }
        #endregion


        #region 
        public async Task<IActionResult> ObjektiExcel()
        {
            var uredaji = await ctx.Uredaj
                .AsNoTracking()
                    .Select(a => new { a.Naziv, a.IdObjekta })
                    .OrderBy(a => a.IdObjekta)
                    .ToListAsync();
            var objekti = await ctx.Objekt
                .AsNoTracking()
                .Include(a => a.IdVrsteObjektaNavigation)
                .Include(a => a.IdDioniceNavigation)
                .OrderBy(a => a.Id)
                .ToListAsync();


            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis objekata";
                excel.Workbook.Properties.Author = "RPPP14-Matej Jurić";
                var worksheet = excel.Workbook.Worksheets.Add("Objekti");

                worksheet.Cells[1, 1].Value = "Id objekta";
                worksheet.Cells[1, 2].Value = "Naziv objekta";
                worksheet.Cells[1, 3].Value = "Vrsta objekta ";
                worksheet.Cells[1, 4].Value = "Lokacija";
                worksheet.Cells[1, 5].Value = "Naziv dionice ";
                worksheet.Cells[1, 6].Value = "Uređaji";

                for (int i = 0; i < objekti.Count; i++)
                {
                    worksheet.Cells[i + 2, 1].Value = objekti[i].Id;
                    worksheet.Cells[i + 2, 2].Value = objekti[i].Naziv;
                    worksheet.Cells[i + 2, 3].Value = objekti[i].IdVrsteObjektaNavigation.Vrsta;
                    worksheet.Cells[i + 2, 4].Value = objekti[i].Lokacija;
                    worksheet.Cells[i + 2, 5].Value = objekti[i].IdDioniceNavigation.Naziv;
          
                    String sveDionice = "";
                    foreach (var uredaj in uredaji)
                    {
                        if (uredaj.IdObjekta == objekti[i].Id)
                        {
                            sveDionice += uredaj.Naziv;
                            sveDionice += "\n";
                        }
                    }
                    worksheet.Cells[i + 2, 6].Value = sveDionice;
                    sveDionice = "";
                   
                }


                worksheet.Cells[1, 1, objekti.Count + 1, 6].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "objekti.xlsx");

        }
        #endregion

        //Izvoz složenih podataka (1:N) u Excel
        #region 
        public async Task<IActionResult> UpraviteljiExcel()
        {
            var autoceste = await ctx.Autocesta
                .AsNoTracking()
                    .Select(a => new { a.Naziv, a.IdUpravitelja })
                    .OrderBy(a => a.IdUpravitelja)
                    .ToListAsync();

            var upravitelji = await ctx.Upravitelj
                .AsNoTracking()
                .OrderBy(a => a.Id)
                .ToListAsync();

            byte[] content;
            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Properties.Title = "Popis Upravitelja";
                excel.Workbook.Properties.Author = "RPPP14-Kristijan";
                var worksheet = excel.Workbook.Worksheets.Add("Upravitelj");

                //First add the headers
                worksheet.Cells[1, 1].Value = "ID";
                worksheet.Cells[1, 2].Value = "Naziv";
                worksheet.Cells[1, 3].Value = "Sjediste ";
                worksheet.Cells[1, 4].Value = "OIB";
                worksheet.Cells[1, 5].Value = "EMail ";
                worksheet.Cells[1, 6].Value = "Kontakt ";
                worksheet.Cells[1, 7].Value = "Autoceste ";

                for (int i = 0; i < upravitelji.Count; i++)
                {
                    worksheet.Cells[i + 2, 1].Value = upravitelji[i].Id;
                    worksheet.Cells[i + 2, 2].Value = upravitelji[i].Naziv;
                    worksheet.Cells[i + 2, 3].Value = upravitelji[i].Sjediste;
                    worksheet.Cells[i + 2, 4].Value = upravitelji[i].Oib.ToString();
                    worksheet.Cells[i + 2, 5].Value = upravitelji[i].EMail;
                    worksheet.Cells[i + 2, 6].Value = upravitelji[i].Kontakt.ToString();

                    String sveAutoceste = "";
                    foreach (var autocesta in autoceste)
                    {
                        if (autocesta.IdUpravitelja == upravitelji[i].Id)
                        {
                            sveAutoceste += autocesta.Naziv;
                            sveAutoceste += "  ";
                            sveAutoceste += "\n";
                        }
                    }
                    worksheet.Cells[i + 2, 7].Value = sveAutoceste;
                    sveAutoceste = "";
                }

                worksheet.Cells[1, 1, autoceste.Count + 1, 7].AutoFitColumns();

                content = excel.GetAsByteArray();
            }
            return File(content, ExcelContentType, "upravitelji.xlsx");
        }
        #endregion






        public async Task<IActionResult> IncidentPdf()
        {
            string naslov = "Popis incidenata";
            var incidenti = await ctx.Incident
                            .AsNoTracking()
                            .Include(d => d.IdUredajaNavigation)
                            .OrderBy(d => d.Id)
                            .ToListAsync();
            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(incidenti));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {
                    column.PropertyName(nameof(Incident.Id));
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id incidenta");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Incident>(x => x.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(2);
                    column.Width(1);
                    column.HeaderCell("Naziv incidenta", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Incident>(x => x.Lokacija);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Lokacija", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Incident>(x => x.Opis);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Opis", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Incident>(x => x.IdUredaja);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(5);
                    column.Width(1);
                    column.HeaderCell("Id uređaja", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Incident>(x => x.IdUredajaNavigation.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(6);
                    column.Width(1);
                    column.HeaderCell("Naziv uređaja", horizontalAlignment: HorizontalAlignment.Center);
                });

               
            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=incidenti.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }





        public async Task<IActionResult> DionicePdf()
        {
            string naslov = "Popis dionica";
            var dionice = await ctx.Dionica
                            .AsNoTracking()
                            .Include (d => d.IdAutocesteNavigation)
                            .OrderBy(d => d.Id)
                            .ToListAsync();
            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
          header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(dionice));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {
                    column.PropertyName(nameof(Dionica.Id));
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id dionice");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Dionica>(x => x.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(2);
                    column.Width(2);
                    column.HeaderCell("Naziv dionice", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Dionica>(x => x.Duljina);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Duljina dionice (m)", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Dionica>(x => x.Pocetak);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Početak dionice", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Dionica>(x => x.Kraj);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(5);
                    column.Width(1);
                    column.HeaderCell("Kraj dionice", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Dionica>(x => x.IdOperatera);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(6);
                    column.Width(1);
                    column.HeaderCell("Id operatera", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<Dionica>(x => x.IdAutocesteNavigation.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(7);
                    column.Width(1);
                    column.HeaderCell("Autocesta", horizontalAlignment: HorizontalAlignment.Center);
                });
            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=dionice.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }

        public async Task<IActionResult> UpraviteljiPdf()
        {
            string naslov = "Popis Upravitelja s pripadajućim autocestama";

            //left outer join 


            var auto2 = await ctx.Upravitelj
                    .AsNoTracking()
                    .Select(a => new UpraviteljPdf
                    {
                        Id = a.Id,
                        Naziv = a.Naziv,
                        Sjediste = a.Sjediste,
                        Oib = a.Oib,
                        EMail = a.EMail,
                        Kontakt = a.Kontakt,

                        AutocesteString = String.Join("\n", ctx.Autocesta.Where(d => d.IdUpravitelja == a.Id).Select(d => d.Naziv).ToList())
                    }).OrderBy(a => a.Id).ToListAsync();

            PdfReport report = CreateReport(naslov);
            #region Podnožje i zaglavlje
            report.PagesFooter(footer =>
            {
                footer.DefaultFooter(DateTime.Now.ToString("dd.MM.yyyy."));
            })
            .PagesHeader(header =>
            {
                header.CacheHeader(cache: true); // It's a default setting to improve the performance.
                header.DefaultHeader(defaultHeader =>
                {
                    defaultHeader.RunDirection(PdfRunDirection.LeftToRight);
                    defaultHeader.Message(naslov);
                });
            });
            #endregion
            #region Postavljanje izvora podataka i stupaca
            report.MainTableDataSource(dataSource => dataSource.StronglyTypedList(auto2));

            report.MainTableColumns(columns =>
            {

                columns.AddColumn(column =>
                {
                    column.PropertyName(nameof(Upravitelj.Id));
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(1);
                    column.HeaderCell("Id upravitelja");
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UpraviteljPdf>(x => x.Naziv);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(1);
                    column.Width(2);
                    column.HeaderCell("Naziv", horizontalAlignment: HorizontalAlignment.Center);
                });



                columns.AddColumn(column =>
                {

                    column.PropertyName<UpraviteljPdf>(a => a.Sjediste);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);

                    column.IsVisible(true);
                    column.Order(3);
                    column.Width(1);
                    column.HeaderCell("Sjediste", horizontalAlignment: HorizontalAlignment.Center);

                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UpraviteljPdf>(x => x.Oib);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("OIB", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UpraviteljPdf>(x => x.EMail);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("EMail", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UpraviteljPdf>(x => x.Kontakt);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(4);
                    column.Width(1);
                    column.HeaderCell("Kontakt", horizontalAlignment: HorizontalAlignment.Center);
                });

                columns.AddColumn(column =>
                {
                    column.PropertyName<UpraviteljPdf>(x => x.AutocesteString);
                    column.CellsHorizontalAlignment(HorizontalAlignment.Center);
                    column.IsVisible(true);
                    column.Order(6);
                    column.Width(2);
                    column.HeaderCell("Autoceste", horizontalAlignment: HorizontalAlignment.Center);
                });

            });

            #endregion
            byte[] pdf = report.GenerateAsByteArray();

            if (pdf != null)
            {
                Response.Headers.Add("content-disposition", "inline; filename=Upravitelji.pdf");
                return File(pdf, "application/pdf");
                //return File(pdf, "application/pdf", "drzave.pdf"); //Otvara save as dialog
            }
            else
                return NotFound();
        }


        public IActionResult Index()
        {
            return View();
        }





    }
}