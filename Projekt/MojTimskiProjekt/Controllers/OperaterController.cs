﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MojTimskiProjekt.Extensions;
using MojTimskiProjekt.Models;
using MojTimskiProjekt.ViewModels;
using Newtonsoft.Json;

namespace MojTimskiProjekt.Controllers
{
    public class OperaterController : Controller
    {
        private readonly RPPP14Context _context;
        private readonly AppSettings appData;
        private readonly ILogger logger;

        public OperaterController(RPPP14Context context, IOptionsSnapshot<AppSettings> options, ILogger<OperaterController> logger)
        {
            _context = context;
            appData = options.Value;
            this.logger = logger;
        }

        // GET: Operater
        public async Task<IActionResult> Index()
        {
            var rPPP14Context = _context.Operater.Include(o => o.IdDioniceNavigation).Include(o => o.IdZaposlenikaNavigation);
            return View(await rPPP14Context.ToListAsync());
        }

        public IActionResult IndexSimple()
        {
            var operateri = _context.Operater
                .Include(z => z.IdZaposlenikaNavigation)
                .Include(z => z.IdDioniceNavigation)
                .AsNoTracking()
                .Select(m => new OperaterViewModel
                {
                    Id = m.Id,
                    IdZaposlenika = m.IdZaposlenika,
                    IdDionice = m.IdDionice,
                    IdZaposlenikaNavigation = m.IdZaposlenikaNavigation,
                    IdDioniceNavigation = m.IdDioniceNavigation
                })
                .ToList();
            var model = new OperateriViewModel
            {
                Operateri = operateri
            };

            // logger.LogInformation("Učitavanje podataka o autocestama");
            return View(model);
        }



        [HttpGet]
        public IActionResult EditPartial(int id)
        {
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Naziv");
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime");
            var operater = _context.Operater
                             .Include(z => z.IdZaposlenikaNavigation)
                             .Include(z => z.IdDioniceNavigation)
                             .AsNoTracking()
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (operater != null)
            {
                return PartialView(operater);
            }
            else
            {
                return NotFound($"Neispravan id operatera: {id}");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditPartial(Operater operater)
        {
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Naziv");
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime");
            if (operater == null)
            {
                return NotFound("Nema poslanih podataka");
            }
            bool checkId = _context.Operater.Any(m => m.Id == operater.Id);
            if (!checkId)
            {
                return NotFound($"Neispravan id operatera: {operater.Id}");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(operater);
                    _context.SaveChanges();
                    logger.LogInformation(new EventId(1000), $"Operater s šifrom {operater.Id} je ažuriran");
                    return StatusCode(302, Url.Action(nameof(Row), new { id = operater.Id }));

                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return PartialView(operater);
                }
            }
            else
            {
                return PartialView(operater);
            }
        }


        public PartialViewResult Row(int id)
        {
            var operater = _context.Operater
                             .Where(m => m.Id == id)
                             .Select(m => new OperaterViewModel
                             {
                                 Id = m.Id,
                                 IdZaposlenika = m.IdZaposlenika,
                                 IdDionice = m.IdDionice,
                                 IdZaposlenikaNavigation = m.IdZaposlenikaNavigation,
                                 IdDioniceNavigation = m.IdDioniceNavigation

                             })
                             .SingleOrDefault();
            if (operater != null)
            {
                return PartialView(operater);
            }
            else
            {
                //vratiti prazan sadržaj?
                return PartialView("ErrorMessageRow", $"Neispravan id operatera: {id}");
            }
        }


        // GET: Operater/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var operater = await _context.Operater
                .Include(o => o.IdDioniceNavigation)
                .Include(o => o.IdZaposlenikaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (operater == null)
            {
                return NotFound();
            }

            return View(operater);
        }

        // GET: Operater/Create
        public IActionResult Create()
        {
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Naziv");
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime");
            return View();
        }

        // POST: Operater/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdDionice,IdZaposlenika")] Operater operater)
        {
            logger.LogTrace(JsonConvert.SerializeObject(operater));
            if (ModelState.IsValid && !OperaterExists(operater.Id))
            {
                
                try
                {
                    _context.Add(operater);
                    await _context.SaveChangesAsync();
                    ViewData["message"] = $"Operater {operater.IdZaposlenikaNavigation.Prezime} je dodan u bazu";

                    logger.LogInformation(new EventId(1000), $"Operater {operater.IdZaposlenikaNavigation.Prezime} je dodan u bazu");
                    return RedirectToAction(nameof(Index), ViewData["message"]);

                } catch (Exception exc)
                {

                    logger.LogError("Pogreška prilikom dodavanje novog operatera: {0}", exc.CompleteExceptionMessage());
                    ModelState.AddModelError(string.Empty, exc.CompleteExceptionMessage());
                    return View(operater);
                }
            }
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Naziv", operater.IdDionice);
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime", operater.IdZaposlenika);
            return View(operater);
        }

        // GET: Operater/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var operater = await _context.Operater.FindAsync(id);
            if (operater == null)
            {
                return NotFound();
            }
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Naziv", operater.IdDionice);
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime", operater.IdZaposlenika);
            return View(operater);
        }

        // POST: Operater/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdDionice,IdZaposlenika")] Operater operater)
        {
            if (id != operater.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(operater);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Operater {operater.IdZaposlenikaNavigation.Prezime} je ažuriran");

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OperaterExists(operater.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdDionice"] = new SelectList(_context.Dionica, "Id", "Naziv", operater.IdDionice);
            ViewData["IdZaposlenika"] = new SelectList(_context.Zaposlenik, "Id", "Prezime", operater.IdZaposlenika);
            return View(operater);
        }

        // GET: Operater/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var operater = await _context.Operater
                .Include(o => o.IdDioniceNavigation)
                .Include(o => o.IdZaposlenikaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (operater == null)
            {
                return NotFound();
            }

            return View(operater);
        }

        // POST: Operater/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var operater = await _context.Operater
                             .Include(b => b.IdDioniceNavigation)
                             .Include(b => b.IdZaposlenikaNavigation)
                             .FirstOrDefaultAsync(m => m.Id == id);
            if (operater != null)
            {
                try
                {
                    string naziv = operater.IdZaposlenikaNavigation.Prezime;
                    _context.Operater.Remove(operater);
                    await _context.SaveChangesAsync();
                    logger.LogInformation($"Operater {naziv} uspješno obrisan");
                } catch (Exception exc)
                {
                    logger.LogError("Pogreška prilikom brisanja operatera: " + exc.CompleteExceptionMessage());
                }
            } else
            {
                logger.LogWarning("Ne postoji operater s id-em: {0} ", id);
            }
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePartial(int id)
        {
            var operater = _context.Operater
                             .Include(b => b.IdDioniceNavigation)
                             .Include(b => b.IdZaposlenikaNavigation)
                             .AsNoTracking() //ima utjecaj samo za Update, za brisanje možemo staviti AsNoTracking
                             .Where(m => m.Id == id)
                             .SingleOrDefault();
            if (operater != null)
            {
                try
                {
                    string naziv = operater.IdZaposlenikaNavigation.Prezime;
                    _context.Remove(operater);
                    _context.SaveChanges();
                    logger.LogInformation($"Operater {naziv} uspješno obrisan");
                    var result = new
                    {
                        message = $"Operater {naziv} sa šifrom {id} obrisan.",
                        successful = true
                    };
                    return Json(result);
                }
                catch (Exception exc)
                {
                    var result = new
                    {
                        message = "Pogreška prilikom brisanja operatera:  " + exc.CompleteExceptionMessage(),
                        successful = false
                    };
                    return Json(result);
                }
            }
            else
            {
                return NotFound($"Operater sa šifrom {id} ne postoji");
            }
        }


        private bool OperaterExists(int id)
        {
            return _context.Operater.Any(e => e.Id == id);
        }
    }
}
