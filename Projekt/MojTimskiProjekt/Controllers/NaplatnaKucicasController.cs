﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MojTimskiProjekt.Models;

namespace MojTimskiProjekt.Controllers
{
    public class NaplatnaKucicasController : Controller
    {
        private readonly RPPP14Context _context;

        public NaplatnaKucicasController(RPPP14Context context)
        {
            _context = context;
        }

        // GET: NaplatnaKucicas
        public IActionResult Index()
        {
            //var racuni = context.Racuni.AsNoTracking().ToList();
            var kucice = _context.NaplatnaKucica.Include(n => n.IdNaplatnePostajeNavigation).ToList();
            var racuni = _context.Racun.AsNoTracking().ToList();
            ViewData["racuni"] = racuni;
            return View(kucice);
        }

        // GET: NaplatnaKucicas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var naplatnaKucica = await _context.NaplatnaKucica
                .Include(n => n.IdNaplatnePostajeNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (naplatnaKucica == null)
            {
                return NotFound();
            }

            return View(naplatnaKucica);
        }

        // GET: NaplatnaKucicas/Create
        public IActionResult Create()
        {
            ViewData["IdNaplatnePostaje"] = new SelectList(_context.NaplatnaPostaja, "Id", "Lokacija");
            return View();
        }

        // POST: NaplatnaKucicas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdNaplatnePostaje,Vrsta,Naziv")] NaplatnaKucica naplatnaKucica)
        {
            if (ModelState.IsValid)
            {
                _context.Add(naplatnaKucica);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdNaplatnePostaje"] = new SelectList(_context.NaplatnaPostaja, "Id", "Lokacija", naplatnaKucica.IdNaplatnePostaje);
            return View(naplatnaKucica);
        }

        // GET: NaplatnaKucicas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var naplatnaKucica = await _context.NaplatnaKucica.FindAsync(id);
            if (naplatnaKucica == null)
            {
                return NotFound();
            }
            ViewData["IdNaplatnePostaje"] = new SelectList(_context.NaplatnaPostaja, "Id", "Lokacija", naplatnaKucica.IdNaplatnePostaje);
            return View(naplatnaKucica);
        }

        // POST: NaplatnaKucicas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdNaplatnePostaje,Vrsta,Naziv")] NaplatnaKucica naplatnaKucica)
        {
            if (id != naplatnaKucica.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(naplatnaKucica);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NaplatnaKucicaExists(naplatnaKucica.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdNaplatnePostaje"] = new SelectList(_context.NaplatnaPostaja, "Id", "Lokacija", naplatnaKucica.IdNaplatnePostaje);
            return View(naplatnaKucica);
        }

        // GET: NaplatnaKucicas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var naplatnaKucica = await _context.NaplatnaKucica
                .Include(n => n.IdNaplatnePostajeNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (naplatnaKucica == null)
            {
                return NotFound();
            }

            return View(naplatnaKucica);
        }

        // POST: NaplatnaKucicas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var naplatnaKucica = await _context.NaplatnaKucica.FindAsync(id);
            _context.NaplatnaKucica.Remove(naplatnaKucica);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NaplatnaKucicaExists(int id)
        {
            return _context.NaplatnaKucica.Any(e => e.Id == id);
        }
    }
}
